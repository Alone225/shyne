import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:progress_hud/progress_hud.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/blocs/authentificationbloc/bloc.dart';
import 'package:shyne/blocs/cartbloc/bloc.dart';
import 'package:shyne/blocs/loginbloc/bloc.dart';
import 'package:shyne/blocs/registerbloc/bloc.dart';
import 'package:shyne/local/persistservice.dart';
import 'package:shyne/model/user.dart';

import 'package:shyne/utils/colors.dart';

class LoginPage extends StatefulWidget {
  LoginPage();
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  AuthentificationBloc _authentificationBloc;
  CartBloc _cartBloc;
  LoginBloc _loginBloc;
  RegisterBloc _registerBloc;
  static Options options = new Options(
      baseUrl: 'http://shyn-e.net/rest/V1/',
      headers: {
        'accept': 'application/json',
        'Content-Type': 'application/json'
      });
  static Options options2 =
      new Options(baseUrl: 'http://shyn-e.net/rest/V1/', headers: {
    'Authorization': ' Bearer ogqjk988owbmfw82chqslin0g54t3s5c',
    'accept': 'application/json'
  });
  Dio dio = new Dio(options);
  Dio dio2 = new Dio();
  TextEditingController email;
  TextEditingController password;

  TextEditingController nom;
  TextEditingController prenom;
  TextEditingController registerEmail;
  TextEditingController registerPassword;
  ProgressHUD _progressHUD;
  bool loginfaild = false;
  bool loading = false;
  Magento magento = Magento();
  final PersistService persistService = PersistService();

  void dismissProgressHUD() {
    setState(() {
      if (loading) {
        _progressHUD.state.dismiss();
      } else {
        _progressHUD.state.show();
      }

      loading = !loading;
    });
  }

  @override
  void initState() {
    _authentificationBloc = BlocProvider.of<AuthentificationBloc>(context);
    _cartBloc = BlocProvider.of<CartBloc>(context);
    _loginBloc = LoginBloc(
        magento: Magento(),
        authentificationBloc: _authentificationBloc,
        cartBloc: _cartBloc);
    _registerBloc = RegisterBloc(magento: Magento(), loginBloc: _loginBloc);
    super.initState();
    email = TextEditingController();
    password = TextEditingController();
    nom = TextEditingController();
    prenom = TextEditingController();
    registerEmail = TextEditingController();
    registerPassword = TextEditingController();
    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: ColorsCustom.secondaryColor,
      containerColor: Colors.white,
      borderRadius: 5.0,
      loading: true,
      text: 'Connexion',
    );
  }

  @override
  void dispose() {
    _loginBloc.dispose();
    _registerBloc.dispose();
    super.dispose();
    email.dispose();
    password.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Theme(
          data: ThemeData(
              brightness: Brightness.light,
              primaryColor: ColorsCustom.primaryColor,
              accentColor: Color(0xFF374250)),
          child: Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                elevation: 0.0,
                title: Text(
                  'Connexion / Inscription',
                  style: TextStyle(fontWeight: FontWeight.w400, fontSize: 14.0),
                ),
                bottom: TabBar(
                  indicatorColor: ColorsCustom.secondaryColor,
                  labelColor: ColorsCustom.secondaryColor,
                  tabs: <Widget>[
                    new Tab(text: "CONNEXION"),
                    new Tab(text: "INSCRIPTION"),
                  ],
                ),
              ),
              body: BlocBuilder(
                bloc: _authentificationBloc,
                builder: (BuildContext context, authstate) {
                  if (authstate is Authenticated) {
                    _onWidgetDidBuild(() {
                      Navigator.pop(context, true);
                    });
                  }
                  return BlocBuilder(
                    bloc: _loginBloc,
                    builder: (BuildContext context, LoginState loginstate) {
                      if (loginstate is Error) {
                        _onWidgetDidBuild(() {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(
                              content: Text('Email ou mot de passe incorrect'),
                              backgroundColor: ColorsCustom.errorColor,
                            ),
                          );
                          _loginBloc.dispatch(InitialState());
                        });
                      }
                      return BlocBuilder(
                        bloc: _registerBloc,
                        builder: (BuildContext context,
                            RegisterState registerstate) {
                          if (registerstate is RegisterError) {
                            _onWidgetDidBuild(() {
                              Scaffold.of(context).showSnackBar(
                                SnackBar(
                                  content: Text(registerstate.message),
                                  backgroundColor: ColorsCustom.errorColor,
                                ),
                              );
                              _registerBloc.dispatch(InitialRegister());
                            });
                          }

                          return Stack(
                            children: <Widget>[
                              Builder(
                                builder: (BuildContext context) {
                                  return TabBarView(
                                    children: <Widget>[
                                      SingleChildScrollView(
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.all(12.0),
                                              width: 250.0,
                                              height: 300.0,
                                              margin:
                                                  EdgeInsets.only(top: 18.0),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              10.0)),
                                                  color: Colors.white,
                                                  boxShadow: [
                                                    BoxShadow(
                                                        color: Colors.grey[300],
                                                        blurRadius: 8.0)
                                                  ]),
                                              child: Column(
                                                children: <Widget>[
                                                  Theme(
                                                    data: Theme.of(context)
                                                        .copyWith(
                                                      primaryColor:
                                                          Color(0xFF374250),
                                                    ),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 16.0,
                                                                  right: 16.0,
                                                                  top: 16),
                                                          child: Text(
                                                              'Identifiant',
                                                              textAlign:
                                                                  TextAlign
                                                                      .left),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                            left: 16.0,
                                                            right: 16.0,
                                                          ),
                                                          child: TextField(
                                                            controller: email,
                                                            cursorColor: Color(
                                                                0xFF374250),
                                                            decoration: InputDecoration(
                                                                contentPadding:
                                                                    EdgeInsets.only(
                                                                        top:
                                                                            4.0,
                                                                        bottom:
                                                                            4.0)),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 16.0,
                                                                  right: 16.0,
                                                                  top: 16.0),
                                                          child: Text(
                                                            'Mot de passe',
                                                            textAlign:
                                                                TextAlign.left,
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 16.0,
                                                                  right: 16.0),
                                                          child: TextField(
                                                            controller:
                                                                password,
                                                            obscureText: true,
                                                            cursorColor: Color(
                                                                0xFF374250),
                                                            decoration: InputDecoration(
                                                                contentPadding:
                                                                    EdgeInsets.only(
                                                                        top:
                                                                            4.0,
                                                                        bottom:
                                                                            4.0)),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 12.0,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    children: <Widget>[
                                                      Text(
                                                        'Mot de passe oublié?',
                                                        textAlign:
                                                            TextAlign.right,
                                                        style: TextStyle(
                                                            color: Colors.blue),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 18.0,
                                                  ),
                                                  ButtonTheme(
                                                    minWidth: 250.0,
                                                    height: 40.0,
                                                    child: RaisedButton(
                                                      onPressed: () {
                                                        _loginBloc.dispatch(
                                                            Login(
                                                                email:
                                                                    email.text,
                                                                password:
                                                                    password
                                                                        .text));
                                                      },
                                                      elevation: 0.0,
                                                      shape: new RoundedRectangleBorder(
                                                          borderRadius:
                                                              new BorderRadius
                                                                      .circular(
                                                                  8.0)),
                                                      color: ColorsCustom
                                                          .primaryColor,
                                                      child: Text(
                                                        'Se connecter',
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      SingleChildScrollView(
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.all(12.0),
                                              width: 250.0,
                                              margin:
                                                  EdgeInsets.only(top: 18.0),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              10.0)),
                                                  color: Colors.white,
                                                  boxShadow: [
                                                    BoxShadow(
                                                        color: Colors.grey[300],
                                                        blurRadius: 8.0)
                                                  ]),
                                              child: Column(
                                                children: <Widget>[
                                                  Theme(
                                                    data: Theme.of(context)
                                                        .copyWith(
                                                      primaryColor:
                                                          Color(0xFF374250),
                                                    ),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 16.0,
                                                                  right: 16.0,
                                                                  top: 16),
                                                          child: Text('Nom',
                                                              textAlign:
                                                                  TextAlign
                                                                      .left),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                            left: 16.0,
                                                            right: 16.0,
                                                          ),
                                                          child: TextField(
                                                            cursorColor: Color(
                                                                0xFF374250),
                                                            controller: nom,
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 16.0,
                                                                  right: 16.0,
                                                                  top: 16),
                                                          child: Text('Prenom',
                                                              textAlign:
                                                                  TextAlign
                                                                      .left),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                            left: 16.0,
                                                            right: 16.0,
                                                          ),
                                                          child: TextField(
                                                            cursorColor: Color(
                                                                0xFF374250),
                                                            controller: prenom,
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 16.0,
                                                                  right: 16.0,
                                                                  top: 16),
                                                          child: Text('Email',
                                                              textAlign:
                                                                  TextAlign
                                                                      .left),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                            left: 16.0,
                                                            right: 16.0,
                                                          ),
                                                          child: TextField(
                                                            cursorColor: Color(
                                                                0xFF374250),
                                                            controller:
                                                                registerEmail,
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 16.0,
                                                                  right: 16.0,
                                                                  top: 16.0),
                                                          child: Text(
                                                            'Mot de passe',
                                                            textAlign:
                                                                TextAlign.left,
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 16.0,
                                                                  right: 16.0),
                                                          child: TextField(
                                                            obscureText: true,
                                                            cursorColor: Color(
                                                                0xFF374250),
                                                            controller:
                                                                registerPassword,
                                                            decoration: InputDecoration(
                                                                contentPadding:
                                                                    EdgeInsets.only(
                                                                        top:
                                                                            4.0,
                                                                        bottom:
                                                                            4.0)),
                                                          ),
                                                        ),
                                                        Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    top: 4,
                                                                    left: 16.0,
                                                                    right:
                                                                        16.0),
                                                            child: Text(
                                                              'Le mot passe doit comporter 8 caractères minimum, au moin un chiffre, une lettre miniscule et et une lettre majuscule.',
                                                              style: TextStyle(
                                                                  fontSize: 10,
                                                                  color: Colors
                                                                      .grey),
                                                            )),
                                                      ],
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 8.0,
                                                  ),
                                                  SizedBox(
                                                    height: 18.0,
                                                  ),
                                                  ButtonTheme(
                                                    minWidth: 250.0,
                                                    height: 40.0,
                                                    child: RaisedButton(
                                                      onPressed: () {
                                                        RegExp regExp =
                                                            new RegExp(
                                                          r"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})",
                                                          caseSensitive: true,
                                                          multiLine: false,
                                                        );
                                                        if (regExp.hasMatch(
                                                            registerPassword
                                                                .text)) {
                                                          var user = User(
                                                            firstname: nom.text,
                                                            lastname:
                                                                prenom.text,
                                                            email: registerEmail
                                                                .text,
                                                          );

                                                          _registerBloc.dispatch(
                                                              Register(
                                                                  user: user,
                                                                  password:
                                                                      registerPassword
                                                                          .text));
                                                        } else {
                                                          Scaffold.of(context)
                                                              .showSnackBar(
                                                                  SnackBar(
                                                            content: Text(
                                                              'Le mot de passe ne respecte pas les contraintes',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                            backgroundColor:
                                                                Colors.red,
                                                          ));
                                                        }
                                                      },
                                                      elevation: 0.0,
                                                      color: ColorsCustom
                                                          .primaryColor,
                                                      shape: new RoundedRectangleBorder(
                                                          borderRadius:
                                                              new BorderRadius
                                                                      .circular(
                                                                  8.0)),
                                                      child: Text(
                                                        'S\'inscrire',
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  );
                                },
                              ),
                              loginstate is Logging ||
                                      registerstate is RegisterLoading
                                  ? _progressHUD
                                  : Container()
                            ],
                          );
                        },
                      );
                    },
                  );
                },
              )),
        ));
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }
}
