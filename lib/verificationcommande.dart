import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:progress_hud/progress_hud.dart';
import 'package:shyne/ajouteradresse.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/editadresse.dart';
import 'package:shyne/model/adresse.dart';
import 'package:shyne/model/orderresume.dart';
import 'package:shyne/model/paymentmethode.dart';
import 'package:shyne/model/shippingmethode.dart';
import 'package:shyne/model/user.dart';
import 'package:shyne/reusmecommande.dart';
import 'package:shyne/utils/colors.dart';

class VerifCommande extends StatefulWidget {
  final User user;
  VerifCommande({this.user});
  @override
  _VerifCommandeState createState() => _VerifCommandeState(user: user);
}

class _VerifCommandeState extends State<VerifCommande> {
  final User user;

  ProgressHUD _progressHUD;
  _VerifCommandeState({this.user});
  Magento magento = Magento();
  Adresse adresse;
  PaymentMethode _paymentMethode;
  List<ShippingMethode> shippingMethodes = List();
  OrderResume _orderResume = OrderResume();
  ShippingMethode _shippingMethode;
  bool loading = true;

  setAdresse() async {
    var add = await magento.getAdresse();
    var usr = await magento.getUser();
    if (add.isNotEmpty) {
      var shippings = await magento.shippingCost(add.last, usr);
      var orderresume = await setShippingInfos(
          add.last,
          usr,
          shippings.isEmpty
              ? ShippingMethode(
                  carriercode: 'flatrate',
                  methodcode: 'flatrate',
                  methodtitle: 'Fixed',
                  amount: 5,
                  carriertitle: 'Flat Rate')
              : shippings.last);

      setState(() {
        adresse = add.last;
        _orderResume = orderresume;
        shippingMethodes.addAll(shippings);
        if (shippingMethodes.isNotEmpty) _shippingMethode = shippingMethodes[1];
        if (orderresume.payments.isNotEmpty) {
          _paymentMethode = orderresume.payments.first;
        }
      });
    }

    setState(() {
      loading = false;
    });
  }

  Future<OrderResume> setShippingInfos(
      Adresse adresse, User user, ShippingMethode shipping) async {
    return await magento.shippingInfos(adresse, user, shipping);
  }

  @override
  void initState() {
    super.initState();
    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: Colors.orangeAccent,
      containerColor: Colors.white,
      loading: true,
      borderRadius: 10.0,
      text: '',
    );
    setAdresse();
  }

  @override
  Widget build(BuildContext context) {
    print(loading);
    return Scaffold(
      appBar: AppBar(
        title: Text('Commander'),
      ),
      body: loading
          ? _progressHUD
          : SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    color: Colors.grey[100],
                    padding: EdgeInsets.all(8.0),
                    margin: EdgeInsets.only(top: 8.0),
                    child: Text(
                      'Adresse de paiement et livraison',
                    ),
                  ),
                  adresse != null
                      ? Card(
                          child: Container(
                              padding: EdgeInsets.all(8),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  ListTile(
                                    title: Text(adresse.firstname +
                                        ' ' +
                                        adresse.lastname),
                                    subtitle: Text(adresse.city +
                                        ', ' +
                                        adresse.adresse.first),
                                    trailing: IconButton(
                                      icon: Icon(EvaIcons.edit2Outline,
                                          color: ColorsCustom.secondaryColor),
                                      onPressed: () {
                                        Navigator.of(context)
                                            .push(MaterialPageRoute(
                                                builder: (context) =>
                                                    EditeAdresse()))
                                            .then((added) {
                                          if (added) {
                                            setState(() {
                                              loading = true;
                                            });
                                            setAdresse();
                                          }
                                        });
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 16),
                                    child: Text(adresse.telephone),
                                  )
                                ],
                              )),
                        )
                      : RaisedButton(
                          elevation: 0.0,
                          textColor: Colors.white,
                          child: Text('Ajouter une adresse'),
                          color: ColorsCustom.secondaryColor,
                          onPressed: () {
                            Navigator.of(context)
                                .push(MaterialPageRoute(
                                    builder: (context) => AjouterAdresse()))
                                .then((added) {
                              if (added) {
                                setState(() {
                                  loading = true;
                                });
                                setAdresse();
                              }
                            });
                          },
                        ),
                  Container(
                    width: double.infinity,
                    color: Colors.grey[100],
                    padding: EdgeInsets.all(8.0),
                    margin: EdgeInsets.only(top: 8.0),
                    child: Text(
                      'Methode de livraison',
                    ),
                  ),
                  Column(
                    children: shippingMethodes
                        .map(
                          (methode) => RadioListTile<ShippingMethode>(
                                title: Text(methode.methodtitle),
                                value: methode,
                                groupValue: _shippingMethode,
                                onChanged: (ShippingMethode value) {
                                  setState(() {
                                    _shippingMethode = value;
                                    print(_shippingMethode.methodcode);
                                  });
                                },
                              ),
                        )
                        .toList(),
                  ),
                  Container(
                    width: double.infinity,
                    color: Colors.grey[100],
                    padding: EdgeInsets.all(8.0),
                    margin: EdgeInsets.only(top: 8.0),
                    child: Text(
                      'Methode de paiement',
                    ),
                  ),
                  Column(
                    children: _orderResume.payments
                        .map(
                          (methode) => RadioListTile<PaymentMethode>(
                                title: Text(methode.title),
                                value: methode,
                                groupValue: _paymentMethode,
                                onChanged: (PaymentMethode value) {
                                  setState(() {
                                    _paymentMethode = value;
                                    print(_paymentMethode.code);
                                  });
                                },
                              ),
                        )
                        .toList(),
                  ),
                  Container(
                    height: 1.0,
                    color: Colors.blueGrey[50],
                  ),
                  adresse == null
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.all(8),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('Produits',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    Text(
                                      _orderResume.total.itemsqty.toString(),
                                    ),
                                  ],
                                )),
                            Container(
                                padding: EdgeInsets.all(8),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('Livraison',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    Text(
                                      '${_orderResume.total.shippingamount} FCFA',
                                    ),
                                  ],
                                )),
                            Container(
                                padding: EdgeInsets.all(8),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('MONTANT TOTAL',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    Text(
                                      '${_orderResume.total.grandtotal} FCFA',
                                    ),
                                  ],
                                )),
                            SizedBox(
                              height: 12,
                            ),
                          ],
                        )
                ],
              ),
            ),
      bottomNavigationBar: Builder(
        builder: (BuildContext context) {
          return Container(
              height: 50,
              color: ColorsCustom.primaryColor,
              padding: EdgeInsets.all(8.0),
              child: InkWell(
                  highlightColor: ColorsCustom.secondaryColor,
                  onTap: () {
                    if (adresse != null) {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ResumeCommande(
                                orderResume: _orderResume,
                                paymentMethode: _paymentMethode,
                              ),
                          fullscreenDialog: true));
                    }
                  },
                  child: Center(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'CONTINUER',
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        )
                      ],
                    ),
                  )));
        },
      ),
    );
  }
}
