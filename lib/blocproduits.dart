import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:progress_hud/progress_hud.dart';
import 'package:shyne/product.dart';
import 'package:shyne/productdetails.dart';
import 'package:shyne/utils/colors.dart';


class BlocProduits extends StatefulWidget {
  final String title;
  final List<int> catsIds;

  const BlocProduits({Key key, this.title, this.catsIds}) : super(key: key);
 
  @override
  _BlocProduitsState createState() =>
      _BlocProduitsState(title, catsIds);
}

class _BlocProduitsState extends State<BlocProduits> {
  List<Produit> _produits = List();
  final String title;
  final List<int> catsIds;
  bool loading=true;
  static Options options =
      new Options(baseUrl: 'http://shyn-e.net/rest/all/V1/', headers: {
    'Authorization': ' Bearer gorh5js19d7pifmp3roauqsi0e900ww1',
    'accept': 'application/json'
  });
  Dio dio = new Dio(options);
  final String imgbaseUrl = "http://shyn-e.net/pub/media/catalog/product";
  ProgressHUD _progressHUD;
  _BlocProduitsState(this.title, this.catsIds);
  Future getall() async{
    for (var id in catsIds) {
      var produits=await getProducts(id);
      setState(() {
       _produits.addAll(produits); 
       if (produits.isNotEmpty) {
            loading=false;                     
       }
      });
    }
    
  }
  Future<List<Produit>> getProducts(int id) async {
    List<Produit> products=[];
    var data=await dio.get('categories/$id/products');
    print(data.data);
    for (var prod in data.data) {
      var sku=prod['sku'];
        try {
          await dio.get('products/$sku').then((value) {
          var item = value.data;
          setState(() {
             products.add(Produit.fromJson(item));
            });
        });
        } catch (e) {
        }
      }
    return products;
  }

  @override
  void initState() {
    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: Colors.orangeAccent,
      containerColor: Colors.white,
      loading: true,
      borderRadius: 10.0,
      text: '',
    );
    getall();

    super.initState();
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Stack(
        children: <Widget>[
          CustomScrollView(
        slivers: <Widget>[
          SliverGrid.count(
                          crossAxisSpacing: 2.0,
                          crossAxisCount: 2,
                          childAspectRatio: 4.5 / 6.1,
                          children: _produits
                              .map((f) => GestureDetector(
                                    child: Card(
                                      
                                      elevation: 0.2,
                                      child: Stack(
                                        children: <Widget>[
                                          Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          AspectRatio(
                                            aspectRatio: 14.0 / 12.0,
                                            child: Image.network(
                                              f.src,
                                              fit: BoxFit.fitWidth,
                                            ),
                                          ),
                                          Expanded(
                                            child: Padding(
                                              padding: EdgeInsets.all(4.0),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  SizedBox(
                                                    height: 10.0,
                                                  ),
                                                  Text(
                                                    f.nom,
                                                    maxLines: 1,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontSize: 12.0),
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                   
                                                  SizedBox(
                                                    height: 6.0,
                                                  ),
                                                  Text(
                                                    '${f.discount>0? f.discount: f.prix}  FCFA',
                                                    style: TextStyle(
                                                        color: ColorsCustom
                                                            .secondaryColor,
                                                        fontSize: 16.0,
                                                        fontWeight:
                                                            FontWeight.w700),
                                                  ),
                                                 
                                                  f.discount>0? Text(
                                                    '${f.prix}  FCFA',
                                                    
                                                    style: TextStyle(
                                                        decoration: TextDecoration.lineThrough,
                                                        color: Colors.grey
                                                        ),
                                                  ):Container()
                                                ],
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      f.discount>0?Positioned(
                                        right: 0,
                                        child: Container(
                                          width: 40,
                                          height: 20,
                                          child: Center(
                                            child: Text('${ (((f.prix-f.discount)*100)/f.prix).round()*-1}%',
                                            style: TextStyle(color: Colors.white, fontSize: 12.0),
                                            ),
                                            
                                          ),
                                          decoration: BoxDecoration(
                                            color: ColorsCustom.discuntBadgeColor,
                                            borderRadius: BorderRadius.only(topRight: Radius.circular(4))
                                          ),
                                        ),

                                      ):Container()
                                        ],
                                      )
                                    ),
                                    onTap: () => Navigator.of(context).push(
                                        new MaterialPageRoute(
                                            builder: (context) =>
                                                ProductDetails(f))),
                                  ))
                              .toList())
        ],
      ),
      loading?_progressHUD:Container()
        ],
      )
    );
  }
}
