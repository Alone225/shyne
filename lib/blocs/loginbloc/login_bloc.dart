import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';

import 'package:meta/meta.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/blocs/authentificationbloc/bloc.dart';
import 'package:shyne/blocs/cartbloc/bloc.dart';
import './bloc.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final Magento magento;
  final AuthentificationBloc authentificationBloc;
  final CartBloc cartBloc;
  LoginBloc({@required this.magento, @required this.authentificationBloc, @required this.cartBloc});
  @override
  LoginState get initialState => InitialLoginState();

  @override
  Stream<LoginState> mapEventToState(
    LoginState currentState,
    LoginEvent event,
  ) async* {
    if (event is Login) {
      try {
        yield Logging();
        var tokken =
            await magento.login(email: event.email, password: event.password);
        authentificationBloc.dispatch(LoggedIn(token: tokken));
        cartBloc.dispatch(FetchNoImages());
        yield InitialLoginState();
      } on DioError catch (e) {
        print(e.response.data);
        yield Error();
      }
    }
    if (event is InitialState) {
      yield InitialLoginState();
    }
  }
}
