import 'package:meta/meta.dart';

abstract class LoginEvent {}
class Login extends LoginEvent{
  final String email;
  final String password;

  Login({@required this.email,@required this.password});

}
class InitialState extends LoginEvent {
  
}