import 'package:shyne/model/cartITem.dart';
import 'package:shyne/product.dart';

abstract class CartEvent {}

class Fetch extends CartEvent {}
class FetchNoImages extends CartEvent {}
class Add extends CartEvent {
   final Produit produit;
  Add(this.produit);
}
class Update extends CartEvent {
   final CartItem cartItem;
  Update(this.cartItem);
}
class UpdateNoImage extends CartEvent {
   final CartItem cartItem;
  UpdateNoImage(this.cartItem);
}
class Remove extends CartEvent {
  final List<CartItem> cartItems;
  Remove(this.cartItems);
}
class Initial extends CartEvent{
  
}