import 'package:shyne/model/cartITem.dart';

abstract class CartState {}
  
class InitialCartblocState extends CartState {}

class LoadingState extends CartState {}

class LoadedState extends CartState {
  List<CartItem> cartItems;
  
  LoadedState(this.cartItems);

  int get total{
    int totalprice=0;
    cartItems.forEach((item){
      totalprice+=item.price*item.qty;
    });
    return totalprice;
  }
  int get productCount{
    int count=0;
    cartItems.forEach((item){
      count+=item.qty;
    });
    return count;
  }
}