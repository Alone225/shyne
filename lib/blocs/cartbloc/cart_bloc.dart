import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/local/persistservice.dart';
import './bloc.dart';

class CartBloc extends Bloc<CartEvent, CartState> {

  final Magento magento;
  final PersistService persistService;
  CartBloc({@required this.magento, @required this.persistService});
  @override
  CartState get initialState => InitialCartblocState();

  @override
  Stream<CartState> mapEventToState(
    CartState currentState,
    CartEvent event,
  ) async* {
    if (event is Fetch) {
      yield LoadingState();
      final cartItems=await magento.getCartItems();
      yield LoadedState(cartItems);
    }
    if (event is FetchNoImages) {
      yield LoadingState();
      final cartItems=await magento.getCartItemsNoImages();
      yield LoadedState(cartItems);
    }
    if (event is Add) {
      yield LoadingState();
      final tokken=await persistService.getToken();
      final cartId=await magento.createCart(tokken);
      final added=await magento.addToCart(tokken, event.produit, cartId);
      final cartItems=await magento.getCartItemsNoImages();
      await magento.createCart(tokken);
      print(cartItems.length);
      yield LoadedState(cartItems);
    }
    if (event is Update) {
      yield LoadingState();
      final tokken=await persistService.getToken();
      final cartId=await magento.createCart(tokken);
      final added=await magento.updateCart(tokken, event.cartItem, cartId);
      final cartItems=await magento.getCartItems();
      yield LoadedState(cartItems);
    }
    if (event is UpdateNoImage) {
      yield LoadingState();
      final tokken=await persistService.getToken();
      final cartId=await magento.createCart(tokken);
      final added=await magento.updateCart(tokken, event.cartItem, cartId);
      final cartItems=await magento.getCartItemsNoImages();
      yield LoadedState(cartItems);
    }
    if (event is Remove) {
      yield LoadingState();
      final tokken=await persistService.getToken();
     for (var item in event.cartItems) {
       await magento.deleteFromCart(tokken, item);
     }
      final cartItems=await magento.getCartItems();
      yield LoadedState(cartItems);
    }
    if (event is Initial) {
      yield InitialCartblocState();
    }
  }
}
