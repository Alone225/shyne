import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:shyne/local/persistservice.dart';
import 'package:shyne/product.dart';
import './bloc.dart';

class WishlistBloc extends Bloc<WishlistEvent, WishlistState> {
  final PersistService _persistService;

  WishlistBloc(this._persistService);
  @override
  WishlistState get initialState => InitialWishlistState();

  @override
  Stream<WishlistState> mapEventToState(
    WishlistState currentState,
    WishlistEvent event,
  ) async* {
    if (event is AddWish) {
      if (currentState is WishlistLoaded) {
        List<String> wishlist = [];
        currentState.produits.add(event.produit);
        for (var produit in currentState.produits) {
            wishlist.add(json.encode(produit.toJson()));
          }
          await _persistService.saveWishList(wishlist);
        yield WishlistLoaded(produits: currentState.produits);
      } else {
        List<Produit> produits = [];
        produits.add(event.produit);
        yield WishlistLoaded(produits: produits);
      }
    }
    if (event is RemoveWish) {
      if (currentState is WishlistLoaded) {
        try {
          List<String> wishlist = [];
          currentState.produits
              .removeWhere((produit) => produit.id == event.produit.id);
          for (var produit in currentState.produits) {
            wishlist.add(json.encode(produit.toJson()));
          }
          await _persistService.saveWishList(wishlist);
          yield WishlistLoaded(produits: currentState.produits);
        } catch (e) {
          
        }
      }
    }
    if (event is FisrtLoad) {
      List<Produit> produits = [];
      try {
        List<String> wishlist = await _persistService.getWishList();
        for (var item in wishlist) {
          produits.add(Produit.fromLocal(json.decode(item)));
        }
        yield WishlistLoaded(produits: produits);
      } catch (e) {
        yield WishlistLoaded(produits: produits);
      }
    }
  }
}
