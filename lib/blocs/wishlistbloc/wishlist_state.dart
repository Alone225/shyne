import 'package:meta/meta.dart';
import 'package:shyne/product.dart';

@immutable
abstract class WishlistState {}
  
class InitialWishlistState extends WishlistState {}

class  WishlistLoaded extends  WishlistState{
  List<Produit> produits=[];

  WishlistLoaded({this.produits});
}