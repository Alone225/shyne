import 'package:meta/meta.dart';
import 'package:shyne/product.dart';

@immutable
abstract class WishlistEvent {}

class AddWish extends WishlistEvent {
  final Produit produit;

  AddWish(this.produit);
}
class RemoveWish extends WishlistEvent {
  final Produit produit;

  RemoveWish(this.produit);
}
class  FisrtLoad extends  WishlistEvent{
  
}