import 'package:meta/meta.dart';
import 'package:shyne/model/user.dart';

abstract class RegisterEvent {}

class Register extends  RegisterEvent {
  final User user;
  final String password;

  Register({@required this.user, @required this.password});
}
class Edit extends  RegisterEvent {
  final User user;
  final String password;

  Edit({@required this.user, @required this.password});
}
class InitialRegister extends RegisterEvent {
  
}