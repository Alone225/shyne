import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';

import 'package:meta/meta.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/blocs/loginbloc/bloc.dart';
import './bloc.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final Magento magento;
  final LoginBloc loginBloc;

  RegisterBloc({@required this.magento, @required this.loginBloc});

  @override
  RegisterState get initialState => InitialRegisterState();

  @override
  Stream<RegisterState> mapEventToState(
    RegisterState currentState,
    RegisterEvent event,
  ) async* {
    if (event is Register) {
      yield RegisterLoading();
      try {
        var created = await magento.createUser(event.user, event.password);

        if (created) {
          loginBloc.dispatch(
              Login(email: event.user.email, password: event.password));
          yield InitialRegisterState();
        } else {
          yield RegisterError('Erreur lors de la connexion');
        }
      } on DioError catch (e) {
        print(e.response.data);
        yield RegisterError(e.response.data['message']);
      }
    }
    if (event is Edit) {
      yield RegisterLoading();
      try {
        var created = await magento.editUser(event.user, event.password);

        if (created) {
          yield InitialRegisterState();
        } else {
          yield RegisterError('Erreur lors de la connexion');
        }
      } on DioError catch (e) {
        print(e.response.data);
        yield RegisterError(e.response.data['message']);
      }
    }
    if (event is InitialRegister) {
      yield InitialRegisterState();
    }
  }
}
