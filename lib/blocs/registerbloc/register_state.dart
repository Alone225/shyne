abstract class RegisterState {}
  
class InitialRegisterState extends RegisterState {}

class RegisterLoading extends RegisterState {}
class RegisterError extends RegisterState {
  String message;
  RegisterError(this.message);
}
