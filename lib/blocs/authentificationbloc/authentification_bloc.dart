import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/local/persistservice.dart';
import './bloc.dart';

class AuthentificationBloc extends Bloc<AuthentificationEvent, AuthentificationState> {

  final Magento magento;
  final PersistService persistService;
  AuthentificationBloc({@required this.magento, @required this.persistService});
  @override
  AuthentificationState get initialState => InitialAuthentificationState();

  @override
  Stream<AuthentificationState> mapEventToState(
    AuthentificationState currentState,
    AuthentificationEvent event,
  ) async* {
    if (event is AppStarted) {
      var tokken =await persistService.getToken();
      if (tokken.isEmpty) {
        yield Unauthenticated();
      }else{
        var user=await magento.getUser();
        yield Authenticated(user);
      }
    }
    if (event is LoggedIn) {
      await persistService.saveToken(event.token);
      var user=await magento.getUser();
      yield Authenticated(user);
    }
    if (event is LoggedOut) {
      var removed=await persistService.removToken();
      if (removed) {
        yield Unauthenticated();
      }
    }
  }
}
