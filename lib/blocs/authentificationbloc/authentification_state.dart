import 'package:shyne/model/user.dart';

abstract class AuthentificationState {}
  
class InitialAuthentificationState extends AuthentificationState {}
class Authenticated extends AuthentificationState {
  final User user;
  Authenticated(this.user);
}

class Unauthenticated extends AuthentificationState {
  
}

class Loading extends AuthentificationState {
  
}