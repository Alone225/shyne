import 'package:meta/meta.dart';

abstract class AuthentificationEvent {}

class AppStarted extends AuthentificationEvent {}

class LoggedIn extends AuthentificationEvent {
  final String token;

  LoggedIn({@required this.token});
}

class LoggedOut extends AuthentificationEvent {}
