import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shyne/cartbutton.dart';
import 'package:shyne/model/category.dart';
import 'package:shyne/productsbycat.dart';

class Categories extends StatefulWidget {
  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  List<Category> categories = List();
  static Options options =
      new Options(baseUrl: 'http://shyn-e.net/rest/all/V1/', headers: {
    'Authorization': ' Bearer ogqjk988owbmfw82chqslin0g54t3s5c',
    'accept': 'application/json'
  });
  final String imgbaseUrl = "http://shyn-e.net/pub/media/catalog/product";
  Dio dio = new Dio(options);
  Future getCategories() async {
    await dio.get('categories', data: {'rootCategoryId': 2, 'depth': 2}).then(
        (data) {
      var datas = data.data['children_data'];
      print(datas);
      datas.forEach((f) {
        print(f);
        setState(() {
          categories.add(Category.fromJson(f));
        });
      });
    });
  }

  @override
  initState() {
    super.initState();
    getCategories();
  }

  Widget _buildTiles(Category root) {
    if (root.subs.isEmpty)
      return ListTile(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ProductsByCat(category: root)));
        },
        title: Text(root.name),
        subtitle: Text('${root.product_count} produit(s)'),
      );
    return ExpansionTile(
      key: PageStorageKey<Category>(root),
      title: Text(root.name),
      children: root.subs.map(_buildTiles).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Catégories'),
          actions: <Widget>[CartButton()],
        ),
        body: ListView.builder(
          itemBuilder: (BuildContext context, int index) =>
              _buildTiles(categories[index]),
          itemCount: categories.length,
        ));
  }
}
