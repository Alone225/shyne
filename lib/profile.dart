import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:progress_hud/progress_hud.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/blocs/authentificationbloc/bloc.dart';
import 'package:shyne/blocs/cartbloc/bloc.dart';
import 'package:shyne/blocs/loginbloc/bloc.dart';
import 'package:shyne/blocs/registerbloc/bloc.dart';
import 'package:shyne/model/user.dart';
import 'package:shyne/utils/colors.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  AuthentificationBloc _authentificationBloc;
  RegisterBloc _registerBloc;
   CartBloc _cartBloc;
  LoginBloc _loginBloc;
  TextEditingController nom;
  TextEditingController prenom;
  TextEditingController registerEmail;
  TextEditingController registerPassword;
  bool firstload = true;
  bool loading = true;
  User user = User();
  Magento magento = Magento();
  ProgressHUD _progressHUD;
  Future<User> getUser() async {
    var _usr = await magento.getUser();
    setState(() {
      print(_usr.email);
      user = _usr;
      nom.text = user.firstname;
      prenom.text = user.lastname;
      registerEmail.text = user.email;
      loading = false;
    });
    return _usr;
  }

  @override
  void initState() {
    getUser();
    _authentificationBloc = BlocProvider.of<AuthentificationBloc>(context);
    _cartBloc=BlocProvider.of<CartBloc>(context);
    _loginBloc = LoginBloc(
        magento: Magento(), authentificationBloc: _authentificationBloc, cartBloc: _cartBloc);
    _registerBloc = RegisterBloc(magento: Magento(), loginBloc: _loginBloc);
    nom = TextEditingController();
    prenom = TextEditingController();
    registerEmail = TextEditingController();
    registerPassword = TextEditingController();
    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: ColorsCustom.secondaryColor,
      containerColor: Colors.white,
      borderRadius: 5.0,
      loading: true,
      text: '',
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Mon profil'),
        ),
        body: BlocBuilder(
          bloc: _authentificationBloc,
          builder: (BuildContext context, state) {
            if (state is Authenticated && !firstload) {
              _onWidgetDidBuild(() {
                setState(() {
                  firstload = true;
                });
              });
            }
            if (state is Authenticated) {
              return Stack(
                children: <Widget>[
                  SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Theme(
                          data: Theme.of(context).copyWith(
                            primaryColor: Color(0xFF374250),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(
                                    left: 16.0, right: 16.0, top: 16),
                                child: Text('Nom ', textAlign: TextAlign.left),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                  left: 16.0,
                                  right: 16.0,
                                ),
                                child: TextField(
                                  cursorColor: Color(0xFF374250),
                                  controller: nom,
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.only(
                                          top: 4.0, bottom: 4.0)),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    left: 16.0, right: 16.0, top: 16),
                                child:
                                    Text('Prénom', textAlign: TextAlign.left),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                  left: 16.0,
                                  right: 16.0,
                                ),
                                child: TextField(
                                  controller: prenom,
                                  cursorColor: Color(0xFF374250),
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.only(
                                          top: 4.0, bottom: 4.0)),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    left: 16.0, right: 16.0, top: 16),
                                child: Text('Email', textAlign: TextAlign.left),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                  left: 16.0,
                                  right: 16.0,
                                ),
                                child: TextField(
                                  controller: registerEmail,
                                  cursorColor: Color(0xFF374250),
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.only(
                                          top: 4.0, bottom: 4.0)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 12.0,
                        ),
                        SizedBox(
                          height: 18.0,
                        ),
                        ButtonTheme(
                          minWidth: 250.0,
                          height: 40.0,
                          child: RaisedButton(
                            onPressed: () {
                              var user = User(
                                id: state.user.id,
                                firstname: nom.text,
                                lastname: prenom.text,
                                email: registerEmail.text,
                              );
                              setState(() {
                                loading = true;
                              });
                              magento.editUser(user, '').then((suucces) {
                                getUser().then((_) {
                                  Scaffold.of(context).showSnackBar(SnackBar(
                                      backgroundColor: Colors.lightGreen,
                                      content: Row(
                                        children: <Widget>[
                                          Icon(EvaIcons.checkmark),
                                          Text("Modificé avec succès")
                                        ],
                                      )));
                                });
                              });
                            },
                            elevation: 0.0,
                            color: ColorsCustom.primaryColor,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(8.0)),
                            child: Text(
                              'Modifier',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  loading ? _progressHUD : Container()
                ],
              );
            }
          },
        ));
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }
}
