import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';

class MsgRetour extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Messages Promos'),
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(30),
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(EvaIcons.gift, color: Colors.grey, size: 42,),
            Text('Aucune promtion en cours pour le moment. Nous enverons des messages quand des promotions serons lancées.',
            textAlign: TextAlign.center,
            style: TextStyle( color: Colors.grey),
            )
          ]
        ),
        )
      ),
    );
  }
}