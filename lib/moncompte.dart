import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shyne/adressview.dart';
import 'package:shyne/blocs/authentificationbloc/bloc.dart';
import 'package:shyne/blocs/cartbloc/bloc.dart';
import 'package:shyne/listecommande.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shyne/local/persistservice.dart';
import 'package:shyne/paramettres.dart';
import 'package:shyne/profile.dart';
import 'package:shyne/wishlist.dart';

class Compte extends StatefulWidget {
  @override
  _CompteState createState() => _CompteState();
}

class _CompteState extends State<Compte> {
  PersistService persistService = PersistService();

  _launchURL() async {
  const url = 'http://shyn-e.net/conditions-d-utilisaton.phtml';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
  @override
  Widget build(BuildContext context) {
    var _authBloc = BlocProvider.of<AuthentificationBloc>(context);
    var cartBloc = BlocProvider.of<CartBloc>(context);
    return Scaffold(
        appBar: AppBar(
          title: Text('Mon compte'),
          elevation: 0.0,
        ),
        backgroundColor: Colors.blueGrey[50],
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 8.0,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => MesCommandes()));
                },
                child: Container(
                  color: Colors.white,
                  child: ListTile(
                    leading: Icon(
                      EvaIcons.archive,
                      color: Colors.grey[400],
                    ),
                    title: Text('Mes commandes'),
                  ),
                ),
              ),
              SizedBox(
                height: 1.0,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => WishList()));
                },
                child: Container(
                  color: Colors.white,
                  child: ListTile(
                    leading: Icon(
                      Icons.favorite_border,
                      color: Colors.grey[400],
                    ),
                    title: Text('Ma liste de souhait'),
                  ),
                ),
              ),
              SizedBox(
                height: 1.0,
              ),
              Container(
                color: Colors.white,
                child: ListTile(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => AdresseView()));
                  },
                  leading: Icon(
                    Icons.location_on,
                    color: Colors.grey[400],
                  ),
                  title: Text('Mon adresse'),
                ),
              ),
              SizedBox(
                height: 1.0,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Profile()));
                },
                child: Container(
                  color: Colors.white,
                  child: ListTile(
                    leading: Icon(
                      Icons.person,
                      color: Colors.grey[400],
                    ),
                    title: Text('Infos du compte'),
                  ),
                ),
              ),
              SizedBox(
                height: 6.0,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Parametres()));
                },
                child: Container(
                  color: Colors.white,
                  child: ListTile(
                    leading: Icon(
                      Icons.settings,
                      color: Colors.grey[400],
                    ),
                    title: Text('Paramètres'),
                  ),
                ),
              ),
              SizedBox(
                height: 1.0,
              ),
              InkWell(
                onTap: () {
                  _launchURL();
                },
                child: Container(
                  color: Colors.white,
                  child: ListTile(
                    leading: Icon(
                      Icons.announcement,
                      color: Colors.grey[400],
                    ),
                    title: Text('Politiques '),
                  ),
                ),
              ),
              SizedBox(
                height: 1.0,
              ),
              /* Container(
                color: Colors.white,
                child: ListTile(
                  leading: Icon(
                    EvaIcons.questionMarkCircleOutline,
                    color: Colors.grey[400],
                  ),
                  title: Text('Aide '),
                ),
              ), */
              SizedBox(
                height: 1.0,
              ),
              InkWell(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            title: Text('Se deconnecter'),
                            content:
                                Text('Voulez vous vraiment vous deconnecter ?'),
                            actions: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Text('Annuler'),
                              ),
                              FlatButton(
                                onPressed: () {
                                  persistService.removToken().then((onValue) {
                                    _authBloc.dispatch(LoggedOut());
                                    cartBloc.dispatch(Initial());
                                    Navigator.of(context)
                                        .popUntil(ModalRoute.withName('/'));
                                  });
                                },
                                child: Text('Oui'),
                              )
                            ],
                          ));
                },
                child: Container(
                  color: Colors.white,
                  child: ListTile(
                    leading: Icon(
                      EvaIcons.logOut,
                      color: Colors.grey[400],
                    ),
                    title: Text('Deconnexion'),
                  ),
                ),
              ),
              SizedBox(
                height: 6.0,
              ),
              Container(
                color: Colors.white,
                child: ListTile(
                  title: Text(
                    " J'accepte de recevoire les offres et promotions de Shyn-e",
                    style: TextStyle(color: Colors.grey, fontSize: 12.0),
                  ),
                  trailing: Switch(
                    onChanged: (bool value) {},
                    value: false,
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
