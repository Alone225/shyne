import 'package:dio/dio.dart';
import 'package:shyne/local/persistservice.dart';
import 'package:shyne/model/Order.dart';
import 'package:shyne/model/adresse.dart';
import 'package:shyne/model/cartITem.dart';
import 'package:shyne/model/orderresume.dart';
import 'package:shyne/model/paymentmethode.dart';
import 'package:shyne/model/pays.dart';
import 'package:shyne/model/shippingmethode.dart';
import 'package:shyne/model/user.dart';
import 'package:shyne/product.dart';

class Magento {
  static Options options =
      new Options(baseUrl: 'http://shyn-e.net/rest/V1/', headers: {
    'Authorization': ' Bearer ogqjk988owbmfw82chqslin0g54t3s5c',
    'Content-Type': 'application/json'
  });
  final String imgbaseUrl = "http://shyn-e.net/pub/media/catalog/product";
  final PersistService persistService = PersistService();
  Dio dio = new Dio();

  Future<List<Produit>> get(String name) async {
    List<Produit> produits = [];
    await dio
        .get('products',
            data: {
              'searchCriteria[currentPage]': 1,
              'searchCriteria[filterGroups][0][filters][0][field]': 'name',
              'searchCriteria[filterGroups][0][filters][0][value]':
                  '%25$name%25',
              'searchCriteria[filterGroups][0][filters][0][conditionType]':
                  'like',
            },
            options: options)
        .then((value) {
      var data = value.data;
      print(data);
      data['items'].forEach((item) {
        produits.add(Produit.fromJson(item));
      });
    });
    return produits;
  }

  Future<List<CartItem>> getCartItems() async {
    List<CartItem> cartItems = List();
    await persistService.getToken().then((tokken) async {
      print(" tokken is $tokken");
      try {
        if (tokken.isNotEmpty) {
          Options cartoptions =
              new Options(baseUrl: 'http://shyn-e.net/rest/V1/', headers: {
            'Authorization': ' Bearer $tokken',
            'accept': 'application/json',
            'Content-Type': 'application/json'
          });
          var data = await dio.get('carts/mine', options: cartoptions);
          print(data.data);
          for (var item in data.data['items']) {
            var cartItem = CartItem.fromJson(item);
            cartItem.images = await this.getProcutImages(cartItem.sku);
            cartItems.add(cartItem);
          }
        } else
          print('Your not logged in');
      } on DioError catch (e) {
        print(e.response.data);
      }
    });

    return cartItems;
  }

  Future<List<CartItem>> getCartItemsNoImages() async {
    List<CartItem> cartItems = List();
    await persistService.getToken().then((tokken) async {
      print(" tokken is $tokken");
      try {
        if (tokken.isNotEmpty) {
          Options cartoptions =
              new Options(baseUrl: 'http://shyn-e.net/rest/V1/', headers: {
            'Authorization': ' Bearer $tokken',
            'accept': 'application/json',
            'Content-Type': 'application/json'
          });
          var data = await dio.get('carts/mine', options: cartoptions);
          print(data.data);
          for (var item in data.data['items']) {
            var cartItem = CartItem.fromJson(item);
            cartItems.add(cartItem);
          }
        } else
          print('Your not logged in');
      } on DioError catch (e) {
        print(e.response.data);
      }
    });

    return cartItems;
  }

  Future<String> createCart(String tokken) async {
    String quoteid = '';
    print(tokken);
    if (tokken.isNotEmpty) {
      Options cartoptions =
          new Options(baseUrl: 'http://shyn-e.net/rest/V1/', headers: {
        'Authorization': ' Bearer $tokken',
        'accept': 'application/json',
        'Content-Type': 'application/json'
      });
      try {
        await dio.post('carts/mine', options: cartoptions).then((data) {
          print(data.data);
          quoteid = data.data;
        });
      } on DioError catch (e) {
        print(e);
      }
    }
    return quoteid;
  }

  Future<bool> updateCart(String tokken, CartItem item, String cartid) async {
    bool status = false;
    if (tokken.isNotEmpty) {
      Options cartoptions =
          new Options(baseUrl: 'http://shyn-e.net/rest/V1/', headers: {
        'Authorization': ' Bearer $tokken',
        'accept': 'application/json',
        'Content-Type': 'application/json'
      });

      try {
        await dio.put('carts/mine/items/${item.item_id}',
            options: cartoptions,
            data: {
              "cartItem": {"sku": item.sku, "qty": item.qty, "quote_id": cartid}
            }).then((data) {
          print(data.data);
        });
      } on DioError catch (e) {
        print(e);
        if (e.response.statusCode == 401) {}
      }
    } else
      print('Your not logged in');
    return status;
  }

  Future<bool> addToCart(String tokken, Produit produit, String cartid) async {
    bool status = false;
    print(tokken);
    if (tokken.isNotEmpty) {
      Options cartoptions =
          new Options(baseUrl: 'http://shyn-e.net/rest/V1/', headers: {
        'Authorization': ' Bearer $tokken',
        'accept': 'application/json',
        'Content-Type': 'application/json'
      });

      try {
        await dio.post('carts/mine/items', options: cartoptions, data: {
          "cartItem": {"sku": produit.sku, "qty": 1, "quote_id": cartid}
        }).then((data) {
          print(data.data);
        });
      } on DioError catch (e) {
        print(e.response.data);
        if (e.response.statusCode == 401) {}
      }
    } else
      print('Your not logged in');
    return status;
  }

  Future<bool> deleteFromCart(String tokken, CartItem item) async {
    bool status = false;
    print(tokken);
    if (tokken.isNotEmpty) {
      Options cartoptions =
          new Options(baseUrl: 'http://shyn-e.net/rest/V1/', headers: {
        'Authorization': ' Bearer $tokken',
        'accept': 'application/json',
        'Content-Type': 'application/json'
      });

      try {
        await dio
            .delete(
          'carts/mine/items/${item.item_id}',
          options: cartoptions,
        )
            .then((data) {
          print(data.data);
        });
      } on DioError catch (e) {
        print(e);
        if (e.response.statusCode == 401) {}
      }
    } else
      print('Your not logged in');
    return status;
  }

  Future<bool> createUser(User user, String password) async {
    bool success = false;
    print(password);
    await dio.post('customers', options: options, data: {
      "customer": {
        "email": user.email,
        "firstname": user.firstname,
        "lastname": user.lastname,
        "storeId": 1,
        "websiteId": 1
      },
      "password": password
    }).then((data) {
      success = true;
      print(data.data);
    });

    return success;
  }

  Future<bool> editUser(User user, String password) async {
    bool success = false;
    print(user.id);
    await dio.put('customers/${user.id}', options: options, data: {
      "customer": {
        'id': user.id,
        "email": user.email,
        "firstname": user.firstname,
        "lastname": user.lastname,
        "storeId": 1,
        "websiteId": 1
      },
      "password": password
    }).then((data) {
      success = true;
    });

    return success;
  }

  Future<User> getUser() async {
    User user = User()..tokken = '';
    await persistService.getToken().then((tokken) async {
      print(" tokken is $tokken");
      if (tokken.isNotEmpty) {
        Options cartoptions = new Options(
            baseUrl: 'http://shyn-e.net/rest/V1/',
            headers: {
              'Authorization': ' Bearer $tokken',
              'accept': 'application/json'
            });

        try {
          await dio.get('customers/me', options: cartoptions).then((data) {
            print(data.data);
            user = User.fromJson(data.data, tokken);
          });
        } on DioError catch (e) {
          if (e.response.statusCode == 401) {
            user.tokken = '';
          }
        }
      } else
        print('Your not logged in');
      user.tokken = tokken;
    });
    return user;
  }

  Future<bool> addAdresseUser(Adresse adresse, User user) async {
    bool succes = false;
    await persistService.getToken().then((tokken) async {
      print(" tokken is $tokken");
      if (tokken.isNotEmpty) {
        Options customeroptions = new Options(
            baseUrl: 'http://shyn-e.net/rest/V1/',
            headers: {
              'Authorization': ' Bearer $tokken',
              'accept': 'application/json'
            });

        try {
          await dio.put('customers/me', options: customeroptions, data: {
            'customer': {
              'email': user.email,
              "firstname": user.firstname,
              "lastname": user.lastname,
              "websiteId": 1,
              'addresses': [
                {
                  "defaultShipping": true,
                  "defaultBilling": true,
                  "customer_id": user.id,
                  "firstname": adresse.firstname,
                  "lastname": adresse.lastname,
                  "region": {"regionCode": null, "region": null, "regionId": 0},
                  "postcode": adresse.postcode,
                  "street": adresse.adresse,
                  "city": adresse.city,
                  "telephone": adresse.telephone,
                  "countryId": adresse.country
                }
              ]
            }
          }).then((data) {
            succes = true;
          });
        } on DioError catch (e) {
          print(e.response.data);
          print(e.response.request.path);
        }
      } else
        print('Your not logged in');
    });
    return succes;
  }

  Future<User> editAdresseUser(Adresse adresse, User user, int id) async {
    await persistService.getToken().then((tokken) async {
      print(" tokken is $tokken");
      print(adresse.lastname);
      if (tokken.isNotEmpty) {
        Options customeroptions = new Options(
            baseUrl: 'http://shyn-e.net/rest/V1/',
            headers: {
              'Authorization': ' Bearer $tokken',
              'accept': 'application/json'
            });

        try {
          await dio.put('customers/me', options: customeroptions, data: {
            'customer': {
              'email': user.email,
              "firstname": user.firstname,
              "lastname": user.lastname,
              "websiteId": 1,
              'addresses': [
                {
                  "id": id,
                  "defaultShipping": true,
                  "defaultBilling": true,
                  "customer_id": user.id,
                  "firstname": adresse.firstname,
                  "lastname": adresse.lastname,
                  "region": {"regionCode": null, "region": null, "regionId": 0},
                  "postcode": adresse.postcode,
                  "street": adresse.adresse,
                  "city": adresse.city,
                  "telephone": adresse.telephone,
                  "countryId": adresse.country
                }
              ]
            }
          }).then((data) {});
        } on DioError catch (e) {
          print(e.response.data);
          print(e.response.request.path);
        }
      } else
        print('Your not logged in');
    });
    return user;
  }

  Future<List<String>> getProcutImages(String sku) async {
    List<String> images = List();
    if (sku.isNotEmpty) {
      try {
        var data = await dio.get('products/$sku/media', options: options);
        print(data.data);
        for (var media in data.data) {
          images.add(imgbaseUrl + media['file']);
        }
      } on DioError catch (e) {
        print('Error get iamges $e');
      }
    }

    return images;
  }

  Future<List<Adresse>> getAdresse() async {
    List<Adresse> adresses = List();
    await persistService.getToken().then((tokken) async {
      Options customeroptions = new Options(
          baseUrl: 'http://shyn-e.net/rest/V1/',
          headers: {
            'Authorization': ' Bearer $tokken',
            'accept': 'application/json'
          });
      try {
        await dio.get('customers/me', options: customeroptions).then((data) {
          data.data['addresses'].forEach((adresse) {
            adresses.add(Adresse.fromJson(adresse));
          });
        });
      } on DioError catch (e) {
        print('Error get adress $e');
      }
    });

    return adresses;
  }

  Future<Adresse> defaultCartAdresse() async {
    Adresse adresse;
    await persistService.getToken().then((tokken) async {
      Options customeroptions = new Options(
          baseUrl: 'http://shyn-e.net/rest/V1/',
          headers: {
            'Authorization': ' Bearer $tokken',
            'accept': 'application/json'
          });
      try {
        await dio.get('carts/mine', options: customeroptions).then((data) {
          if (data.data['customer']['addresses'].isNotEmpty) {
            adresse = Adresse.fromJson(data.data['customer']['addresses'].last);
          }
        });
      } on DioError catch (e) {
        print('Error get adress $e');
      }
    });
    return adresse;
  }

  Future<List<ShippingMethode>> shippingCost(Adresse adresse, User user) async {
    List<ShippingMethode> shippings = List();

    await persistService.getToken().then((tokken) async {
      Options customeroptions = new Options(
          baseUrl: 'http://shyn-e.net/rest/V1/',
          headers: {
            'Authorization': ' Bearer $tokken',
            'accept': 'application/json'
          });
      try {
        await dio.post('carts/mine/estimate-shipping-methods',
            options: customeroptions,
            data: {
              "address": {
                "region": "null",
                "region_id": 0,
                "region_code": "null",
                "country_id": adresse.country,
                "street": adresse.adresse,
                "postcode": adresse.postcode,
                "city": adresse.city,
                "firstname": adresse.firstname,
                "lastname": adresse.lastname,
                "customer_id": user.id,
                "email": user.email,
                "telephone": adresse.telephone,
                "same_as_billing": 1
              }
            }).then((data) {
          print(data.data);
          data.data.forEach((shipping) {
            shippings.add(ShippingMethode.fromJson(shipping));
          });
        });
      } on DioError catch (e) {
        print(e.response.data);
      }
    });

    return shippings;
  }

  Future<OrderResume> shippingInfos(
      Adresse adresse, User user, ShippingMethode shipping) async {
    OrderResume orderResume = OrderResume();
    await persistService.getToken().then((tokken) async {
      Options customeroptions = new Options(
          baseUrl: 'http://shyn-e.net/rest/V1/',
          headers: {
            'Authorization': ' Bearer $tokken',
            'accept': 'application/json'
          });
      try {
        print(shipping.carriercode);
        print(shipping.methodcode);
        await dio.post('carts/mine/shipping-information',
            options: customeroptions,
            data: {
              "addressInformation": {
                "shipping_address": {
                  "region": "null",
                  "region_id": 0,
                  "region_code": "null",
                  "country_id": adresse.country,
                  "street": adresse.adresse,
                  "postcode": adresse.postcode,
                  "city": adresse.city,
                  "firstname": adresse.firstname,
                  "lastname": adresse.lastname,
                  "customer_id": user.id,
                  "email": user.email,
                  "telephone": adresse.telephone,
                },
                "billing_address": {
                  "region": "null",
                  "region_id": 0,
                  "region_code": "null",
                  "country_id": adresse.country,
                  "street": adresse.adresse,
                  "postcode": adresse.postcode,
                  "city": adresse.city,
                  "firstname": adresse.firstname,
                  "lastname": adresse.lastname,
                  "customer_id": user.id,
                  "email": user.email,
                  "telephone": adresse.telephone,
                },
                "shipping_carrier_code": shipping.carriercode,
                "shipping_method_code": shipping.methodcode
              }
            }).then((data) {
          print(data.data);
          data.data['payment_methods'].forEach((payment) {
            orderResume.payments.add(PaymentMethode.fromJson(payment));
          });
          orderResume.total = Total.fromJson(data.data['totals']);
        });
      } on DioError catch (e) {
        print(e.response.data);
      }
    });

    return orderResume;
  }

  Future<String> createOrder(
      Adresse adresse, PaymentMethode payment, User user) async {
    String orderid = '';
    await persistService.getToken().then((tokken) async {
      Options customeroptions = new Options(
          baseUrl: 'http://shyn-e.net/rest/default/V1/',
          headers: {
            'Authorization': ' Bearer $tokken',
            'accept': 'application/json'
          });
      try {
        await dio.post('carts/mine/payment-information',
            options: customeroptions,
            data: {
              "paymentMethod": {"method": payment.code},
              "billing_address": {
                "email": user.email,
                "region": "null",
                "region_id": 0,
                "region_code": "null",
                "country_id": adresse.country,
                "street": adresse.adresse,
                "postcode": adresse.postcode,
                "city": adresse.city,
                "telephone": adresse.telephone,
                "firstname": adresse.firstname,
                "lastname": adresse.lastname
              }
            }).then((data) {
          print(data.data);
          orderid = data.data;
        });
      } on DioError catch (e) {
        print('Error get adress $e');
        print(e.response.data);
      }
    });
    return orderid;
  }

  Future<Order> getOrder(String id) async {
    Order order = Order();
    try {
      await dio.get('orders/$id', options: options).then((data) {
        order = Order.fromJson(data.data);
      });
    } on DioError catch (e) {
      print(e.message);
    }
    return order;
  }

  Future<List<Order>> getOrders(String id) async {
    List<Order> orders = List();
    try {
      await dio.get('orders', options: options, data: {
        'searchCriteria[filterGroups][0][filters][0][field]': 'customer_id',
        'searchCriteria[filterGroups][0][filters][0][value]': id,
        'searchCriteria[sortOrders][0][field]': 'updated_at',
        'searchCriteria[sortOrders][0][direction]': 'DESC'
      }).then((data) {
        print(data);
        data.data['items'].forEach((order) {
          orders.add(Order.fromJson(order));
        });
      });
    } on DioError catch (e) {
      print(e.response.data);
    }
    return orders;
  }

  Future<List<Pays>> getCountries() async {
    List<Pays> pays = List();
    try {
      Options countriesoptions = new Options(
          baseUrl: 'http://shyn-e.net/rest/V1/',
          headers: {'Content-Type': 'application/json'});
      await dio
          .get('directory/countries', options: countriesoptions)
          .then((data) {
        print(data.data);
        data.data.forEach((countrie) {
          var c = Pays.fromJson(countrie);
          if (c.namelocale != null || c.nameeng != null) pays.add(c);
        });
      });
    } on DioError catch (e) {
      print(e.response.data);
    }
    return pays;
  }

  Future<String> login({String email, String password}) async {
    String tokken = '';

    Options logoptions = new Options(
        baseUrl: 'http://shyn-e.net/rest/V1/',
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json'
        });
    await dio
        .post('integration/customer/token',
            data: {'username': email, 'password': password},
            options: logoptions)
        .then((data) async {
      print(data.statusCode);
      if (data.statusCode == 200) {
        print(data.data);
        tokken = data.data;
      }
    });
    return tokken;
  }

  Future<bool> checkEmail(String email) async {
    bool available = false;
    try {
      Options checkemailoptions = new Options(
          baseUrl: 'http://shyn-e.net/rest/V1/',
          headers: {
            'Content-Type': 'application/json',
            'accept': 'application/json'
          });
      await dio.post('customers/isEmailAvailable',
          options: checkemailoptions,
          data: {"customerEmail": email, "websiteId": 0}).then((data) {
        print(data.data);
        available = data.data;
      });
    } on DioError catch (e) {
      print(e.response.data);
    }
    return available;
  }
}
