import 'package:flutter/material.dart';

class FullScreenImagePage extends StatefulWidget {
  final List<dynamic> images;
  final int page;
  FullScreenImagePage(this.images,this.page);

  @override
  FullScreenImagePageSate createState() {
    
    return FullScreenImagePageSate(images, page);
  }
}
class FullScreenImagePageSate extends State<FullScreenImagePage>{

  var images;
  var page;
  FullScreenImagePageSate(this.images,this.page);
  PageController pageController;

  @override
  void initState() {
    super.initState();
    pageController=new PageController(initialPage: page);
    
  }
  @override
  void dispose() {
    super.dispose();
    pageController.dispose();
  }
  @override
  Widget build(BuildContext context) {
  
    return new Scaffold(
      body: new SizedBox.expand(
        child: new Container(
          decoration: new BoxDecoration(color: Colors.black),
          child: new Stack(
            children: <Widget>[
              new Align(
                alignment: Alignment.center,
                child: Container(
                  height: 400.0,
                  child: new Hero(
                  tag: 'produit',
                  child: PageView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: images.length,
                    controller: pageController,
                    itemBuilder: (context,i){
                      return new Image(
                          image: images[i],
                      );
                    },
                    onPageChanged: (i){
                        setState(() => page=i);
                    },

                  )
                ),
                )
              ),
              new Align(
                alignment: Alignment.topCenter,
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new AppBar(
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      leading: new IconButton(
                        icon: new Icon(
                          Icons.close,
                          color: Colors.white,
                        ),
                        onPressed: () => Navigator.of(context).pop(),
                      ),
                    )
                  ],
                ),
              ),
              Align(
                alignment: Alignment(0.0,0.7),
                child: Text((page+1).toString()+"/"+images.length.toString(),style: 
                TextStyle(color: Colors.white ,fontSize: 18.0),),
              )
            ],
          ),
        ),
      ),
    );
    // TODO: implement build
  }

}