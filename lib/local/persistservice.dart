import 'package:shared_preferences/shared_preferences.dart';

class PersistService {
  final String tokenKey = "TOKEN";
  final String wishlistkey='wishlist';
  Future<bool> saveToken(String token) async {
    var pref = await SharedPreferences.getInstance();
    bool saved = await pref.setString(tokenKey, token );
    return saved;
  }

  Future<String> getToken() async {
    var pref = await SharedPreferences.getInstance();
    var token =  pref.getString(tokenKey);
    return token ?? '';
  }
  Future<bool> removToken() async {
    var pref = await SharedPreferences.getInstance();
    var status = await pref.remove(tokenKey);
    return status;
  }

  Future<bool> saveWishList(List<String> wishlist) async{
    var pref = await SharedPreferences.getInstance();
    var status=await pref.setStringList(wishlistkey, wishlist);
    return status;
  }
  Future<List<String>> getWishList() async{
    var pref = await SharedPreferences.getInstance();
    List<String> wishlist=  pref.getStringList(wishlistkey);

    return wishlist;
  }
}
