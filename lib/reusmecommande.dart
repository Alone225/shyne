
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:progress_hud/progress_hud.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/blocs/cartbloc/bloc.dart';
import 'package:shyne/custompackages/fullscreenmodal.dart';
import 'package:shyne/model/adresse.dart';
import 'package:shyne/model/cartITem.dart';
import 'package:shyne/model/orderresume.dart';
import 'package:shyne/model/paymentmethode.dart';
import 'package:shyne/model/user.dart';
import 'package:shyne/utils/colors.dart';

class ResumeCommande extends StatefulWidget {
  final OrderResume orderResume;
 final PaymentMethode paymentMethode;

  ResumeCommande({this.orderResume, this.paymentMethode});
  @override
  _ResumeState createState() =>
      _ResumeState(orderResume: orderResume, paymentMethode: paymentMethode);
}

class _ResumeState extends State<ResumeCommande> {
  OrderResume orderResume;
  PaymentMethode paymentMethode;
  ProgressHUD _progressHUD;
  _ResumeState({this.orderResume, this.paymentMethode});
  Magento magento = Magento();

  CartBloc _cartBloc;

  User user;
  List<CartItem> cartItems;
  Adresse adresse;
  bool loading = true;

  getAll() async {
    var _user = await magento.getUser();
    var _adresses = await magento.getAdresse();
    var _items = await magento.getCartItems();
    setState(() {
      user = _user;
      adresse = _adresses.last;
      cartItems = _items;
      loading = false;
    });
  }

  deleteItem(
    List<CartItem> items,
    String tokken,
  ) async {
    setState(() {
      loading = true;
    });
    items.forEach((item) async {
      await magento.deleteFromCart(tokken, item);
    });
  }

  @override
  void initState() {
    _cartBloc = BlocProvider.of<CartBloc>(context);
    getAll();
    super.initState();
    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: Colors.orangeAccent,
      containerColor: Colors.white,
      loading: true,
      borderRadius: 10.0,
      text: '',
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('RÉSUMÉ DE LA COMMANDE'),
      ),
      body: BlocBuilder(
        bloc: _cartBloc,
        builder: (BuildContext context, CartState state) {
          if (!loading && state is LoadedState) {
            return Stack(
              children: <Widget>[
                SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        color: Colors.grey[100],
                        padding: EdgeInsets.all(8.0),
                        margin: EdgeInsets.only(top: 8.0),
                        child: Text(
                          '${state.cartItems.length} Article(s) dans votre panier',
                        ),
                      ),
                      Column(
                        children: state.cartItems.map((item) {
                          return Row(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.all(6.0),
                                child: Image.network(
                                  item.images.isEmpty? '' : item.images.first,
                                  width: 60.0,
                                  height: 70.0,
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                      child: Container(
                                    width: 230,
                                    child: Text(
                                      item.name + ' ${item.qty}',
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w200),
                                    ),
                                  )),
                                  Text(
                                    '${item.price} FCFA',
                                    style: TextStyle(
                                        color: ColorsCustom.secondaryColor,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ],
                              )
                            ],
                          );
                        }).toList(),
                      ),
                      Container(
                          padding: EdgeInsets.all(8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Sous Total du Panier',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              Text(
                                state.total.toString() + ' FCFA',
                              ),
                            ],
                          )),
                      Container(
                          padding: EdgeInsets.all(8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Produits',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              Text(
                                orderResume.total.itemsqty.toString(),
                              ),
                            ],
                          )),
                      Container(
                          padding: EdgeInsets.all(8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Livraison',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              Text(
                                orderResume.total.shippingamount.toString() +
                                    ' FCFA',
                              ),
                            ],
                          )),
                      Container(
                          padding: EdgeInsets.all(8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Paiement',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              Text(
                                paymentMethode.title,
                              ),
                            ],
                          )),
                      Container(
                          padding: EdgeInsets.all(8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Reduction',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              Text(
                                orderResume.total.discount.toString() + ' FCFA',
                              ),
                            ],
                          )),
                      Container(
                        width: double.infinity,
                        color: Colors.grey[100],
                        padding: EdgeInsets.all(8.0),
                        margin: EdgeInsets.only(top: 8.0),
                        child: Text(
                          'Appliquer un code coupon',
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(8),
                        width: 250,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            TextField(
                              cursorColor: ColorsCustom.primaryColor,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(10),
                                  hintText: 'Entrez le code de réduction',
                                  border: new OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(6.0),
                                    ),
                                  ),
                                  suffixIcon: IconButton(
                                      color: ColorsCustom.primaryColor,
                                      icon: Icon(Icons.check),
                                      onPressed: () {})),
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: 1.0,
                        color: Colors.blueGrey[50],
                      ),
                      SizedBox(
                        height: 22,
                      ),
                      Container(
                          padding: EdgeInsets.all(8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('TOTAL DE LA COMMANDE	',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14)),
                              Text('${orderResume.total.grandtotal} FCFA',
                                  style: TextStyle(fontSize: 14)),
                            ],
                          )),
                    ],
                  ),
                ),
                loading ? _progressHUD : Container()
              ],
            );
          } else
            return _progressHUD;
        },
      ),
      bottomNavigationBar: BlocBuilder(
        bloc: _cartBloc,
        builder: (BuildContext context, CartState state) {
          return Builder(
        builder: (BuildContext context) {
          return Container(
              height: 50,
              color: ColorsCustom.primaryColor,
              padding: EdgeInsets.all(8.0),
              child: InkWell(
                  highlightColor: ColorsCustom.secondaryColor,
                  onTap: () {
                    setState(() {
                      loading=true;
                    });
                    magento
                        .createOrder(adresse, paymentMethode, user)
                        .then((data) async {
                      var order = await magento.getOrder(data);
                      setState(() {
                       loading=false; 
                      });
                      Navigator.of(context)
                          .push(CustomModal(order))
                          .then((value) {
                            _cartBloc.dispatch(FetchNoImages());
                        _onWidgetDidBuild((){
                          Navigator.popUntil(context, ModalRoute.withName('/'));
                        });
                      });
                    });
                  },
                  child: Center(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'PASSEZ LA COMMANDE',
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        )
                      ],
                    ),
                  )));
        },
      );
        },
      ),
    );
  }
  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }
}
