import 'package:flutter/material.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/bloc/productbloc.dart';
import 'package:shyne/productdetails.dart';

class ProductSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ProductBloc productBloc = ProductBloc(Magento());

    return Scaffold(
      appBar: AppBar(
        title: Directionality(
            textDirection: Directionality.of(context),
            child: new TextField(
              cursorColor: Colors.white,
              keyboardType: TextInputType.text,
              style: new TextStyle( fontSize: 16.0, color: Colors.white),
              decoration: new InputDecoration(
                  hintText: 'Rechercher un produit',
                  hintStyle: new TextStyle( fontSize: 16.0),
                  border: InputBorder.none),
              onChanged: productBloc.query.add,
              autofocus: true,
            )),
      ),
      body: Column(
        children: <Widget>[
          StreamBuilder(
            stream: productBloc.log,
            builder: (context, snapshot) => Container(
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(snapshot?.data ?? '')),
                ),
          ),
          Flexible(
            child: StreamBuilder(
              stream: productBloc.results,
              builder: (context, snapshot) {
                if (!snapshot.hasData)
                  return Center(
                    child: CircularProgressIndicator(),
                  );

                return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) => ListTile(
                        leading: CircleAvatar(
                          child: Image.network(snapshot.data[index].src ?? ""),
                          backgroundColor: Colors.white,
                        ),
                        title: Text(
                          snapshot.data[index].nom,
                          maxLines: 2,
                        ),
                        subtitle: Text(snapshot.data[index].prix.toString()+' FCFA'
                            ),
                        onTap: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ProductDetails(
                                      snapshot.data[index] )));
                        },
                      ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
