import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shyne/model/category.dart';
import 'package:shyne/product.dart';
import 'package:shyne/productdetails.dart';
import 'package:shyne/utils/colors.dart';

class ProductsByCat extends StatefulWidget {
  final Category category;
  ProductsByCat({this.category});
  @override
  _ProductsByCatState createState() =>
      _ProductsByCatState(category: this.category);
}

class _ProductsByCatState extends State<ProductsByCat> {
  List<Produit> _produits = List();
  Category category;
  _ProductsByCatState({this.category});

  static Options options =
      new Options(baseUrl: 'http://shyn-e.net/rest/all/V1/', headers: {
    'Authorization': ' Bearer ogqjk988owbmfw82chqslin0g54t3s5c',
    'accept': 'application/json'
  });
  Dio dio = new Dio(options);
  final String imgbaseUrl = "http://shyn-e.net/pub/media/catalog/product";
  Future getProducts() async {
    await dio.get('categories/${category.id}/products').then((prods) {
      var data = prods.data;
      print(data);
      data.forEach((prod) {
        var sku = prod['sku'];
        dio.get('products/$sku').then((value) {
          var item = value.data;
          setState(() {
            _produits.add(Produit.fromJson(item));
          });
        });
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getProducts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(category.name),
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverGrid.count(
              crossAxisSpacing: 2.0,
              crossAxisCount: 2,
              childAspectRatio: 4.5 / 6.1,
              children: _produits
                  .map((f) => GestureDetector(
                        child: Card(
                            elevation: 0.2,
                            child: Stack(
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    AspectRatio(
                                      aspectRatio: 14.0 / 12.0,
                                      child: Image.network(
                                        f.src,
                                        fit: BoxFit.fitWidth,
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: EdgeInsets.all(4.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: <Widget>[
                                            SizedBox(
                                              height: 10.0,
                                            ),
                                            Text(
                                              f.nom,
                                              maxLines: 1,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(fontSize: 12.0),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            SizedBox(
                                              height: 6.0,
                                            ),
                                            Text(
                                              '${f.discount > 0 ? f.discount : f.prix}  FCFA',
                                              style: TextStyle(
                                                  color: ColorsCustom
                                                      .secondaryColor,
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                            f.discount > 0
                                                ? Text(
                                                    '${f.prix}  FCFA',
                                                    style: TextStyle(
                                                        decoration:
                                                            TextDecoration
                                                                .lineThrough,
                                                        color: Colors.grey),
                                                  )
                                                : Container()
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                f.discount > 0
                                    ? Positioned(
                                        right: 0,
                                        child: Container(
                                          width: 40,
                                          height: 20,
                                          child: Center(
                                            child: Text(
                                              '${(((f.prix - f.discount) * 100) / f.prix).round() * -1}%',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12.0),
                                            ),
                                          ),
                                          decoration: BoxDecoration(
                                              color: ColorsCustom
                                                  .discuntBadgeColor,
                                              borderRadius: BorderRadius.only(
                                                  topRight:
                                                      Radius.circular(4))),
                                        ),
                                      )
                                    : Container()
                              ],
                            )),
                        onTap: () => Navigator.of(context).push(
                            new MaterialPageRoute(
                                builder: (context) => ProductDetails(f))),
                      ))
                  .toList())
        ],
      ),
    );
  }
}
