import 'package:flutter/material.dart';
import 'package:shyne/listecommande.dart';
import 'package:shyne/model/Order.dart';

import 'package:shyne/utils/colors.dart';

class CustomModal extends ModalRoute<void> {

  Order order;
  
  CustomModal(this.order);
  @override
  Duration get transitionDuration => Duration(milliseconds: 400);

  @override
  bool get opaque => false;

  @override
  bool get barrierDismissible => false;

  @override
  Color get barrierColor => Colors.black.withOpacity(0.5);

  @override
  String get barrierLabel => null;

  @override
  bool get maintainState => true;

  @override
  Widget buildPage(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
  ) {
    // This makes sure that text and other content follows the material style
    return Material(
      type: MaterialType.transparency,
      // make sure that the overlay content is not cut off
      child: SafeArea(
        child: _buildOverlayContent(context),
      ),
    );
  }

  Widget _buildOverlayContent(BuildContext context) {
    
    return Container(
        color: Colors.white,
        height: 300,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image.asset(
                'assets/success.png',
                width: 100,
                height: 100,
              ),
              SizedBox(
                height: 12,
              ),
              Text(
                'Votre commande a été enregistrée avec succès !',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14, color: Colors.grey),
              ),
              Text.rich( TextSpan(children: [
                TextSpan(
                  text: '${order.itemcount} produits ',
                ),
                TextSpan(
                  text: '${order.montantglobal} FCFA',
                  style: TextStyle( color: ColorsCustom.secondaryColor)
                )
              ])),
              SizedBox(
                height: 12,
              ),
              Text.rich( TextSpan(children: [
                TextSpan(
                  text: 'CODE : ',
                  style: TextStyle( fontSize: 18, fontWeight: FontWeight.bold)
                ),
                TextSpan(
                  text: '${order.ref}',
                  style: TextStyle( fontSize: 18)
                )
              ])),
              SizedBox(
                height: 30,
              ),
              SizedBox(
                  width: 200,
                  child: OutlineButton(
                      child: new Text("Voir mes commandes"),
                      color: ColorsCustom.secondaryColor,
                      highlightedBorderColor: Colors.white,
                      textTheme: ButtonTextTheme.normal,
                      onPressed: ()=> Navigator.pushAndRemoveUntil(context,MaterialPageRoute(builder: (context)=> MesCommandes()), ModalRoute.withName('/')),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0))))
            ],
          ),
        ));
  }

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    // You can add your own animations for the overlay content
    return FadeTransition(
      opacity: animation,
      child: ScaleTransition(
        scale: animation,
        child: child,
      ),
    );
  }
}
