import 'package:flutter/material.dart';

class ColorsCustom {
  static const Color primaryColor=Color(0xFF374250);
  static const Color secondaryColor=Color(0xFFff9900);
  static const Color firdColors= Color(0xFFb12704);
  static const Color countDownColor =Color(0xFFfab45a);
  static const Color errorColor=Color(0xFFe02b27);
  static const Color discuntBadgeColor=Color(0xFFEF5350);
}
