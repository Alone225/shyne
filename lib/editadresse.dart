import 'package:flutter/material.dart';
import 'package:progress_hud/progress_hud.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/model/adresse.dart';
import 'package:shyne/model/pays.dart';
import 'package:shyne/model/user.dart';
import 'package:shyne/utils/colors.dart';

class EditeAdresse extends StatefulWidget {
  @override
  _EditeAdresseState createState() => _EditeAdresseState();
}

class _EditeAdresseState extends State<EditeAdresse> {
  Magento magento = Magento();
  List<Pays> pays = List();
  Pays _pays = Pays();
  Adresse currentadresse;
  ProgressHUD _progressHUD;

  TextEditingController nom;
  TextEditingController prenom;
  TextEditingController telephone;
  TextEditingController adresse;
  TextEditingController ville;
  TextEditingController codepostal;
  User user = User();
  bool loading = true;
  getCountries() async {
    var countries = await magento.getCountries();
    var _usr= await magento.getUser();
    var add = await magento.getAdresse();
    setState(() {
      pays.addAll(countries);
      currentadresse = add.last;
      user= _usr;
      _pays = pays.firstWhere((pays)=> pays.id==currentadresse.country);
      nom.text = currentadresse.firstname;
      prenom.text =currentadresse.lastname;
      telephone.text=currentadresse.telephone;
      adresse.text=currentadresse.adresse.first;
      ville.text=currentadresse.city;
      codepostal.text=currentadresse.postcode;
      print(_pays.namelocale);
      loading = false;
    });
  }
  
  @override
  void initState() {
    
    nom = TextEditingController();
    prenom = TextEditingController();
    telephone = TextEditingController( );
    adresse = TextEditingController();
    ville = TextEditingController();
    codepostal = TextEditingController();
    getCountries();
    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: ColorsCustom.secondaryColor,
      containerColor: Colors.white,
      borderRadius: 5.0,
      loading: true,
      text: '',
    );
    super.initState();
    
  }

  @override
  void dispose() {
    nom.dispose();
    prenom.dispose();
    telephone.dispose();
    adresse.dispose();
    ville.dispose();
    codepostal.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Modifer une adresse'),
        ),
        body: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Theme(
                    data: Theme.of(context).copyWith(
                      primaryColor: Color(0xFF374250),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding:
                              EdgeInsets.only(left: 16.0, right: 16.0, top: 16),
                          child: Text('Nom ', textAlign: TextAlign.left),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            left: 16.0,
                            right: 16.0,
                          ),
                          child: TextField(
                            cursorColor: Color(0xFF374250),
                            controller: nom,
                            decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(top: 4.0, bottom: 4.0)),
                          ),
                        ),
                        Padding(
                          padding:
                              EdgeInsets.only(left: 16.0, right: 16.0, top: 16),
                          child: Text('Prénom', textAlign: TextAlign.left),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            left: 16.0,
                            right: 16.0,
                          ),
                          child: TextField(
                            cursorColor: Color(0xFF374250),
                            controller: prenom,
                            decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(top: 4.0, bottom: 4.0)),
                          ),
                        ),
                        Padding(
                          padding:
                              EdgeInsets.only(left: 16.0, right: 16.0, top: 16),
                          child: Text('Numéro de téléphone ',
                              textAlign: TextAlign.left),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            left: 16.0,
                            right: 16.0,
                          ),
                          child: TextField(
                            cursorColor: Color(0xFF374250),
                            keyboardType: TextInputType.phone,
                            controller: telephone,
                            decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(top: 4.0, bottom: 4.0)),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 16.0, right: 16.0, top: 16.0),
                          child: Text(
                            'Adresse',
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16.0, right: 16.0),
                          child: TextField(
                            cursorColor: Color(0xFF374250),
                            controller: adresse,
                            decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(top: 4.0, bottom: 4.0)),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 16.0, right: 16.0, top: 16.0),
                          child: Text(
                            'Code postal',
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16.0, right: 16.0),
                          child: TextField(
                            cursorColor: Color(0xFF374250),
                            controller: codepostal,
                            decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(top: 4.0, bottom: 4.0)),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 16.0, right: 16.0, top: 16.0),
                          child: Text(
                            'Ville',
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16.0, right: 16.0),
                          child: TextField(
                            cursorColor: Color(0xFF374250),
                            controller: ville,
                            decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(top: 4.0, bottom: 4.0)),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 16.0, right: 16.0, top: 16.0),
                          child: Text(
                            'Pays',
                            textAlign: TextAlign.left,
                          ),
                        ),
                        pays.isEmpty
                            ? Container()
                            : Padding(
                                padding:
                                    EdgeInsets.only(left: 16.0, right: 16.0),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton<Pays>(
                                    value: _pays,
                                    onChanged: (Pays newValue) {
                                      setState(() {
                                        _pays = newValue;
                                      });
                                    },
                                    items: pays.map<DropdownMenuItem<Pays>>(
                                        (Pays value) {
                                      return DropdownMenuItem<Pays>(
                                        value: value,
                                        child: Text(
                                            value.namelocale ?? value.nameeng),
                                      );
                                    }).toList(),
                                  ),
                                ))
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 12.0,
                  ),
                  SizedBox(
                    height: 18.0,
                  ),
                  ButtonTheme(
                    minWidth: 250.0,
                    height: 40.0,
                    child: RaisedButton(
                      onPressed: () {
                        setState(() {
                          loading = true;
                        });
                        var adresse = Adresse();
                        setState(() {
                          adresse.firstname = nom.text;
                          adresse.lastname = prenom.text;
                          adresse.city = ville.text;
                          adresse.adresse = [this.adresse.text];
                          adresse.country = _pays.id;
                          adresse.telephone=telephone.text;
                          adresse.postcode = codepostal.text;
                        });

                        magento.editAdresseUser(adresse, user, currentadresse.id).then((onValue) {
                          setState(() {
                            loading = false;
                          });
                          Navigator.pop(context, true);
                        });
                      },
                      elevation: 0.0,
                      color: ColorsCustom.primaryColor,
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8.0)),
                      child: Text(
                        'Modifer',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  )
                ],
              ),
            ),
            loading ? _progressHUD : Container()
          ],
        ));
  }
}
