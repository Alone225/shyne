import 'package:flutter/material.dart';
import 'package:shyne/custompackages/flippanel.dart';
import 'package:shyne/utils/colors.dart';

class ReverseCountdown extends StatelessWidget {
  
  //when using reverse countdown in your own app, change debugMode to false and provide the requied dDay values.
 
  @override
  Widget build(BuildContext context) {
    return Container(
     height: 50.0,
     padding: EdgeInsets.only(top: 8.0),
      width: double.infinity,
      color: ColorsCustom.countDownColor,
        child: Center(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 6.0),
              child: Text('Se termine dans', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
            ),
            FlipClock.reverseCountdown(
            duration: Duration(hours: 23),
            digitColor: Colors.white,
            backgroundColor: Colors.black,
            digitSize: 12.0,
            height:18.0 ,
            width: 15.0,
            borderRadius: const BorderRadius.all(Radius.circular(3.0)),
            //onDone: () => print('ih'),
          ),
          ],
        ),
        )
      );
  }
}