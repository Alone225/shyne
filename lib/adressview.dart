import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:shyne/ajouteradresse.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/editadresse.dart';
import 'package:shyne/model/adresse.dart';
import 'package:shyne/utils/colors.dart';

class AdresseView extends StatefulWidget {
  @override
  _AdresseViewState createState() => _AdresseViewState();
}

class _AdresseViewState extends State<AdresseView> {
  List<Adresse> adresses = List();
  Magento magento = Magento();
  bool loading = true;
  getAdresses() async {
    var add = await magento.getAdresse();
    setState(() {
      adresses.addAll(add);
      loading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getAdresses();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Mon adresse'),
        ),
        body: loading == false && adresses.isEmpty
            ? Center(child: Text('Vous n\'avez pas encore ajouter d\'adresse'))
            : SingleChildScrollView(
                child: Column(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    color: Colors.grey[100],
                    padding: EdgeInsets.all(8.0),
                    margin: EdgeInsets.only(top: 8.0),
                    child: Text(
                      'Adresse',
                    ),
                  ),
                  Column(
                    children: adresses
                        .map((adresse) => Card(
                        
                              elevation: 0.0,
                              child: Container(
                                  padding: EdgeInsets.all(8),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      ListTile(
                                        title: Text(adresse.firstname +
                                            ' ' +
                                            adresse.lastname),
                                        subtitle: Text(adresse.city +
                                            ', ' +
                                            adresse.adresse.first),
                                        trailing: IconButton(
                                          icon: Icon(EvaIcons.edit2Outline,
                                              color:
                                                  ColorsCustom.secondaryColor),
                                          onPressed: () {
                                            Navigator.of(context)
                                                .push(MaterialPageRoute(
                                                    builder: (context) =>
                                                        EditeAdresse()))
                                                .then((added) {
                                              if (added) {
                                                setState(() {
                                                  loading = true;
                                                  adresses.clear();
                                                  getAdresses();
                                                });
                                              }
                                            });
                                          },
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(left: 16),
                                        child: Text(adresse.telephone),
                                      )
                                    ],
                                  )),
                            ))
                        .toList(),
                  )
                ],
              )),
        bottomNavigationBar: adresses.isEmpty
            ? Builder(
                builder: (BuildContext context) {
                  return Container(
                      height: 50,
                      color: ColorsCustom.primaryColor,
                      padding: EdgeInsets.all(8.0),
                      child: InkWell(
                          highlightColor: ColorsCustom.secondaryColor,
                          onTap: () {
                            Navigator.of(context)
                                .push(MaterialPageRoute(
                                    builder: (context) => AjouterAdresse()))
                                .then((added) {
                              if (added) {
                                setState(() {
                                  loading = true;
                                  adresses.clear();
                                  getAdresses();
                                });
                              }
                            });
                          },
                          child: Center(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Icon(
                                  EvaIcons.plus,
                                  color: Colors.white,
                                ),
                                Text(
                                  'Ajouter une adresse',
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                          )));
                },
              )
            : null);
  }
}
