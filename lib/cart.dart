import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/blocs/cartbloc/bloc.dart';
import 'package:shyne/model/cartITem.dart';
import 'package:shyne/product.dart';
import 'package:progress_hud/progress_hud.dart';
import 'package:shyne/utils/colors.dart';
import 'package:shyne/verificationcommande.dart';

class Cart extends StatefulWidget {
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  List<Produit> products = List();
  List<Produit> selectedprod = List();
  bool loading = true;
  ProgressHUD _progressHUD;
  List<CartItem> selectedItems = List();
  Magento magento;
  static Options options =
      new Options(baseUrl: 'http://shyn-e.net/rest/all/V1/', headers: {
    'Authorization': ' Bearer gorh5js19d7pifmp3roauqsi0e900ww1',
    'accept': 'application/json'
  });
  final String imgbaseUrl = "http://shyn-e.net/pub/media/catalog/product";
  Dio dio = new Dio(options);

  CartBloc _cartBloc;

  @override
  void initState() {
    _cartBloc = BlocProvider.of<CartBloc>(context);
    _cartBloc.dispatch(Fetch());
    super.initState();
    magento = Magento();

    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: Colors.orangeAccent,
      containerColor: Colors.white,
      loading: true,
      borderRadius: 10.0,
      text: '',
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Panier'),
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                _cartBloc.dispatch(Remove(selectedItems));
              },
            )
          ],
        ),
        backgroundColor: Colors.blueGrey[50],
        body: BlocBuilder(
          bloc: _cartBloc,
          builder: (BuildContext context, CartState state) {
            if (state is LoadedState && state.cartItems.isEmpty) {
              return Stack(
                children: <Widget>[
                  Center(
                      child: Text('Votre Panier est vide'),
                    )
                ],
              );
            }
            if (state is LoadingState) {
              return Stack(
                children: <Widget>[
                  Center(
                      child: _progressHUD,
                    )
                ],
              );
            }

            if (state is LoadedState && state.cartItems.isNotEmpty) {
              return Stack(
                children: <Widget>[
                  SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Column(
                            mainAxisSize: MainAxisSize.min,
                            children: state.cartItems
                                .map((item) => Container(
                                    color: Colors.white,
                                    margin: EdgeInsets.only(bottom: 1.0),
                                    padding:
                                        EdgeInsets.only(left: 2.0, right: 4.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        selectedItems.indexWhere((it) =>
                                                    it.item_id ==
                                                    item.item_id) ==
                                                -1
                                            ? GestureDetector(
                                                child: Container(
                                                  margin: EdgeInsets.all(2.0),
                                                  width: 20.0,
                                                  height: 20.0,
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color:
                                                              Colors.grey[400]),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  20.0))),
                                                ),
                                                onTap: () {
                                                  setState(() {
                                                    selectedItems.add(item);
                                                  });
                                                },
                                              )
                                            : GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    selectedItems.remove(item);
                                                  });
                                                },
                                                child: Container(
                                                  width: 20.0,
                                                  height: 20.0,
                                                  margin: EdgeInsets.all(2.0),
                                                  padding: EdgeInsets.all(4),
                                                  child: Center(
                                                    child: Icon(
                                                      Icons.check,
                                                      size: 14.0,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                  decoration: BoxDecoration(
                                                      color: Colors.redAccent,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  20.0))),
                                                )),
                                        Container(
                                          margin: EdgeInsets.all(6.0),
                                          child: Image.network(
                                            item.images.isEmpty ? '':item.images.first,
                                            width: 80.0,
                                            height: 90.0,
                                            fit: BoxFit.fitHeight,
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                                child: Container(
                                              width: 230,
                                              child: Text(
                                                item.name,
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.w200),
                                              ),
                                            )),
                                            Text(
                                              '${item.price} FCFA',
                                              style: TextStyle(
                                                  color: ColorsCustom
                                                      .secondaryColor,
                                                  fontWeight: FontWeight.w600),
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Container(
                                              width: 150.0,
                                              height: 30.0,
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  GestureDetector(
                                                    onTap: () {
                                                      if (item.qty>1) {
                                                        item.qty-=1;
                                                      }
                                                      _cartBloc.dispatch(
                                                          Update(item));
                                                    },
                                                    child: Container(
                                                      width: 40.0,
                                                      decoration: BoxDecoration(
                                                          border: Border.all(
                                                              color: Colors
                                                                  .grey[300]),
                                                          borderRadius:
                                                              BorderRadius.only(
                                                                  topLeft: Radius
                                                                      .circular(
                                                                          2.0),
                                                                  bottomLeft: Radius
                                                                      .circular(
                                                                          2.0))),
                                                      child: Center(
                                                        child: Icon(
                                                          Icons.remove,
                                                          size: 12.0,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    child: Container(
                                                        decoration: BoxDecoration(
                                                            border: Border.all(
                                                                color:
                                                                    Colors.grey[
                                                                        300])),
                                                        child: Center(
                                                          child: Text(item.qty
                                                              .toString()),
                                                        )),
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      item.qty+=1;
                                                      _cartBloc.dispatch(
                                                          Update(item));
                                                    },
                                                    child: Container(
                                                      width: 40.0,
                                                      decoration: BoxDecoration(
                                                          border: Border
                                                              .all(
                                                                  color: Colors
                                                                          .grey[
                                                                      300]),
                                                          borderRadius:
                                                              BorderRadius.only(
                                                                  topRight: Radius
                                                                      .circular(
                                                                          2.0),
                                                                  bottomRight: Radius
                                                                      .circular(
                                                                          2.0))),
                                                      child: Center(
                                                        child: Icon(
                                                          Icons.add,
                                                          size: 12.0,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        )
                                      ],
                                    )))
                                .toList()),
                      ],
                    ),
                  ),
                ],
              );
            }
          },
        ),
        bottomNavigationBar: BlocBuilder(
          bloc: _cartBloc,
          builder: (BuildContext context, CartState state) {
            return Container(
                height: 60.0,
                color: Colors.white,
                padding: EdgeInsets.only(left: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text.rich(TextSpan(children: [
                          TextSpan(text: 'Total: '),
                          TextSpan(
                              text: '${state is LoadedState? state.total:0}',
                              style: TextStyle(
                                  color: ColorsCustom.secondaryColor,
                                  fontWeight: FontWeight.w600)),
                        ])),
                        Text(
                          'Produits: ${state is LoadedState? state.productCount:0}',
                          style: TextStyle(color: Colors.grey, fontSize: 12.0),
                        ),
                        Text('Livraison: 0.00 Fcfa',
                            style:
                                TextStyle(color: Colors.grey, fontSize: 12.0)),
                      ],
                    ),
                    GestureDetector(
                        onTap: () {
                          if (state is LoadedState && state.cartItems.isNotEmpty) {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => VerifCommande()));
                          }
                        },
                        child: Container(
                          width: 100.0,
                          height: 60.0,
                          child: Center(
                              child: Text('Acheter',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold))),
                          decoration: BoxDecoration(color: Color(0xFF374250)),
                        ))
                  ],
                ),
              );
          },
        ));
  }
}
