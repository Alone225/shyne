import 'package:flutter/material.dart';
import 'package:progress_hud/progress_hud.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/cartbutton.dart';
import 'package:shyne/datailscommance.dart';
import 'package:shyne/model/Order.dart';

class MesCommandes extends StatefulWidget {
  @override
  _MesCommandesState createState() => _MesCommandesState();
}

class _MesCommandesState extends State<MesCommandes> {
  Magento magento=Magento();
  List<Order> _orders=List();
  ProgressHUD _progressHUD;
  bool loading=true;
  getOrders() async{
    var user=await magento.getUser();
    var orders=await  magento.getOrders(user.id.toString());
    setState(() {
     _orders.addAll(orders); 

     loading=false;
    });
  }

  @override
  void initState() {
    super.initState();
    getOrders();
    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: Colors.orangeAccent,
      containerColor: Colors.white,
      loading: true,
      borderRadius: 10.0,
      text: '',
    );
  }
  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      appBar: AppBar(
        title: Text('Mes commandes'),
        actions: <Widget>[
          CartButton()
        ],
      ),
      body: loading? _progressHUD:
      _orders.length<1? Center( child: Text("Vous n'avez pas encore passer de commande"),):
        SingleChildScrollView(
          child: Column(
            children: _orders.map((order){
              return InkWell(
                onTap: (){
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context)=>
                      DetailsCommande(order)
                    )
                  );
                },
                child: Container(
                  padding: EdgeInsets.all(8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('#${order.ref}', style: TextStyle( fontWeight: FontWeight.bold),),
                      Text(order.date),
                      Text('Payer par ${order.paiement}'),
                      Container(
                        margin: EdgeInsets.only(top: 2),
                        height: 1,
                        color: Colors.grey[300],
                      )
                    ],
                  ),
                ),
              );
            }).toList(),
          ),
        )
    );
  }
}