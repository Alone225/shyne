import 'package:flutter/material.dart';
import 'package:shyne/product.dart';
import 'package:shyne/utils/colors.dart';

class Seventhlist extends StatefulWidget {
  final List<Produit> produits;

  const Seventhlist({Key key, this.produits}) : super(key: key);
  @override
  _SeventhlistState createState() => _SeventhlistState(produits: produits);
}

class _SeventhlistState extends State<Seventhlist> {
  final List<Produit> produits;

  _SeventhlistState({this.produits});
  @override
  void initState() {
    super.initState();
  }

  buildItem(BuildContext context, int index) {
    return Container(
      height: MediaQuery.of(context).size.height / 3.5,
      child: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height / 7,
            width: MediaQuery.of(context).size.width / 4,
            child: Image(
              image: NetworkImage(produits[index].src),
            ),
          ),
          Text(
            produits[index].nom,
            maxLines: 1,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 12.0),
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(
            height: 6.0,
          ),
          Text(
            '${produits[index].discount > 0 ? produits[index].discount : produits[index].prix}  FCFA',
            style: TextStyle(
                color: ColorsCustom.secondaryColor,
                fontSize: 16.0,
                fontWeight: FontWeight.w700),
          ),
          produits[index].discount > 0
              ? Text(
                  '${produits[index].prix}  FCFA',
                  style: TextStyle(
                      decoration: TextDecoration.lineThrough,
                      color: Colors.grey),
                )
              : Container(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height / 1.6,
      child: Stack(
        children: <Widget>[
          Container(
            height: size.height / 1.7,
            color: Color(0xffF5E4D2),
          ),
          Container(
            height: size.height / 7,
            width: size.width,
            alignment: Alignment.topCenter,
            //child: Image.asset("assets/banner_two.png"),
          ),
          Positioned(
            top: 15,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Smartphones',
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 8),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Color(0xFF374250)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Voir tout',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  width: size.width,
                ),
                Container(
                  padding: EdgeInsets.only(left: 8, right: 8, top: 8),
                  width: size.width,
                  height: size.height / 1.75,
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    color: Colors.white,
                    child: GridView.builder(
                      padding: EdgeInsets.all(10),
                      shrinkWrap: true,
                      itemCount: produits.length,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return buildItem(context, index);
                      },
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
