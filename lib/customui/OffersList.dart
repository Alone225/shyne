import 'package:flutter/material.dart';
import 'package:shyne/ProuductsByCatId.dart';
import 'package:shyne/productsbycat.dart';

class Secondlist extends StatefulWidget {
  @override
  _SecondlistState createState() => _SecondlistState();
}

class ListItem {
  final String image;
  final int catid;

  ListItem({this.image, this.catid});
}

class _SecondlistState extends State<Secondlist> {
  List<ListItem> images = [
    ListItem(image: 'https://www.shyn-e.net/pub/media/wysiwyg/thumbnail_COSMETIQUE.jpg',
    catid: 2),
     ListItem(image: 'https://www.shyn-e.net/pub/media/wysiwyg/bloggif_5c9c8077e5009.jpeg',
    catid: 2),
     ListItem(image: 'https://www.shyn-e.net/pub/media/wysiwyg/IMG-20191031-WA0115.jpg',
    catid: 2),
     ListItem(image: 'https://www.shyn-e.net/pub/media/wysiwyg/Bloc_lectrom_nager.jpg',
    catid: 2),
     ListItem(image: 'https://www.shyn-e.net/pub/media/wysiwyg/TELEVISEUr.jpg',
    catid: 2)
  ];
  buildItem(BuildContext context, int index) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => ProductByCatId(categoryId: images[index].catid,)));
      },
      child: Container(
        height: MediaQuery.of(context).size.height / 4,
        child: Padding(
          padding: const EdgeInsets.only(left: 2, right: 2),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(6),
            child: Image(
              image: NetworkImage(images[index].image),
              height: MediaQuery.of(context).size.height / 4,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(3),
      child: Container(
        height: MediaQuery.of(context).size.height / 4,
        child: ListView.builder(
          itemCount: images.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return buildItem(context, index);
          },
        ),
      ),
    );
  }
}
