import 'package:flutter/widgets.dart';
import 'package:shyne/api/magento.dart';
import 'productbloc.dart';



class ProductProvider extends InheritedWidget {
  final ProductBloc productBloc;

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static ProductBloc of(BuildContext context) =>
      (context.inheritFromWidgetOfExactType(ProductProvider) as ProductProvider)
          .productBloc;

  ProductProvider({Key key, ProductBloc productBloc, Widget child})
      : this.productBloc = productBloc ?? ProductBloc(Magento()),
        super(child: child, key: key);
}