

import 'dart:async';

import 'package:shyne/api/magento.dart';
import 'package:shyne/product.dart';
import 'package:rxdart/rxdart.dart';



class ProductBloc {
  final Magento api;

  Stream<List<Produit>> _results = Stream.empty();
  Stream<String> _log = Stream.empty();

  ReplaySubject<String> _query = ReplaySubject<String>();

  Stream<String> get log => _log;
  Stream<List<Produit>> get results => _results;
  Sink<String> get query => _query;

  ProductBloc(this.api) {
    _results = _query.distinct().asyncMap(api.get).asBroadcastStream();

    _log = Observable(results)
        .withLatestFrom(_query.stream, (_, query) => results.isEmpty != null?'Résutat pour $query':'Résutat pour $query')
        .asBroadcastStream();
  }

  void dispose() {
    _query.close();
  }
}