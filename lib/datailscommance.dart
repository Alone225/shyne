import 'package:flutter/material.dart';
import 'package:shyne/model/Order.dart';

class DetailsCommande extends StatefulWidget {
  final Order order;

  DetailsCommande(this.order);

  @override
  _DetailsCommandeState createState() => _DetailsCommandeState(order);
}

class _DetailsCommandeState extends State<DetailsCommande> {
  final Order order;

  _DetailsCommandeState(this.order);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Details commande')),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                width: double.infinity,
                color: Colors.grey[100],
                padding: EdgeInsets.all(4.0),
                child: ListTile(
                  title: Text(
                    'Commande #${order.ref}',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(order.status),
                      Text('Date de commande ${order.date}'),
                      Text('Frais de port ${order.shippingcost} FCFA'),
                      Text('Reduction ${order.reduction} FCFA'),
                      Text('Montant global ${order.montantglobal} FCFA')
                    ],
                  ),
                )),
            Padding(
              padding: EdgeInsets.all(4),
              child: Text(
                'Articles commandés',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: order.items.map((item) {
                  return ListTile(
                    title: Text(item.name),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('Prix'),
                            Text(item.price.toString() + ' FCFA')
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('Qté'),
                            Text(item.qty.toString())
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Sous total',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Text(
                              item.soustotal.toString() + ' FCFA',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            )
                          ],
                        )
                      ],
                    ),
                  );
                }).toList()),
            Padding(
              padding: EdgeInsets.all(4),
              child: Text(
                'ADRESSE',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(order.adresse.firstname+' '+order.adresse.lastname),
                  Text(order.adresse.adresse.first),
                  Text(order.adresse.city+','+ order.adresse.postcode),
                  Text(order.adresse.country),
                  Text('T: '+ order.adresse.telephone)
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8),
              child: Text(
                'METHODE DE LIVRAISON',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8),
              child: Text(
                order.livraison,
                
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8),
              child: Text(
                'METHODE DE PAIEMENT',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8),
              child: Text(
                order.paiement,
                
              ),
            ),

          ],
        ),
      ),
    );
  }
}
