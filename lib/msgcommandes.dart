import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';

class MsgCommande extends StatelessWidget {
  
  List orders=[];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Messages commandes'),
      ),
      body: orders.isEmpty? Center(
        child: Container(
          padding: EdgeInsets.all(30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(EvaIcons.bell, size: 42, color: Colors.grey,),
            Text('Vous n\'avez aucun message.',
            textAlign: TextAlign.center,
            style: TextStyle( color: Colors.grey),
            )
          ]
        ),
        )
      ):
      SingleChildScrollView(
        child: Column(
          children: orders.map((f){

            return Container(
              padding: EdgeInsets.all(8.0),
              child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Commande en cour de traitement', style: TextStyle( fontWeight: FontWeight.bold),),
                  ],
                ),
                Text.rich( TextSpan(
                  children: [
                    TextSpan(text: 'Bonjour. Votre commande ', style: TextStyle(fontSize: 12.0,color: Colors.black45)),
                    TextSpan(text:  f.ref.toString(), style: TextStyle( fontWeight: FontWeight.bold, fontSize: 12)),
                    TextSpan(text: '  est en cours de traiment nous appelerons ou vous enverons un mail des qu\'elle sera traiter', style: TextStyle(fontSize: 12.0,color: Colors.black45)),
                  ]
                ))
              ],
            )
            );
          }).toList(),
        ),
      )
    );
  }
}