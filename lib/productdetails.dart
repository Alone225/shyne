import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:progress_hud/progress_hud.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/blocs/cartbloc/bloc.dart';
import 'package:shyne/blocs/authentificationbloc/bloc.dart';
import 'package:shyne/blocs/wishlistbloc/bloc.dart';
import 'package:shyne/cartbutton.dart';
import 'package:shyne/customCaroussel.dart';
import 'package:shyne/login.dart';
import 'package:shyne/product.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:share/share.dart';

import 'package:shyne/utils/colors.dart';

class ProductDetails extends StatefulWidget {
  final Produit product;
  ProductDetails(this.product);

  @override
  _ProductDetailsState createState() => _ProductDetailsState(this.product);
}

class _ProductDetailsState extends State<ProductDetails> {
  final Produit produit;

  CartBloc _cartBloc;
  WishlistBloc _wishlistBloc;
  AuthentificationBloc _authentificationBloc;
  final _random = new Random();

  _ProductDetailsState(this.produit);
  int next(int min, int max) => min + _random.nextInt(max - min);
  Magento magento = Magento();
  ProgressHUD _progressHUD;
  bool loading = false;
  List<String> images = [];

  getProductImages() async {
    var imgs = await magento.getProcutImages(produit.sku);
    setState(() {
      images.addAll(imgs);
    });
  }

  bool firtsload = true;
  @override
  void initState() {
    getProductImages();
    _cartBloc = BlocProvider.of<CartBloc>(context);
    _authentificationBloc = BlocProvider.of<AuthentificationBloc>(context);
    _wishlistBloc = BlocProvider.of<WishlistBloc>(context);
    super.initState();
    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: Colors.orangeAccent,
      containerColor: Colors.white,
      loading: true,
      borderRadius: 10.0,
      text: '',
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocBuilder(
          bloc: _cartBloc,
          builder: (BuildContext context, CartState state) {
            if (state is LoadedState && !firtsload) {
              _onWidgetDidBuild(() {
                Scaffold.of(context).showSnackBar(SnackBar(
                    backgroundColor: Colors.lightGreen,
                    content: Row(
                      children: <Widget>[
                        Icon(EvaIcons.checkmark),
                        Text("Produit ajouté avec succès")
                      ],
                    )));
                setState(() {
                  firtsload = true;
                });
              });
            }
            return Stack(
              children: <Widget>[
                CustomScrollView(
                  slivers: <Widget>[
                    SliverAppBar(
                        expandedHeight: 200.0,
                        floating: false,
                        pinned: true,
                        centerTitle: true,
                        titleSpacing: 0.0,
                        elevation: 0.0,
                        actions: <Widget>[CartButton()],
                        flexibleSpace: FlexibleSpaceBar(
                            centerTitle: true,
                            background: images.isEmpty
                                ? Image.network(produit.src, fit: BoxFit.cover)
                                : new CarouselProduct(
                                    images: images.map((link) {
                                      return NetworkImage(link);
                                    }).toList(),
                                    dotSize: 4.0,
                                    dotSpacing: 12.0,
                                    autoplay: false,
                                    dotColor: Colors.white,
                                    indicatorBgPadding: 2.0,
                                    dotBgColor: Colors.transparent,
                                  ))),
                    SliverToBoxAdapter(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: 8.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: widget.product.discount > 0
                                  ? Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          '${widget.product.discount} FCFA',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color:
                                                  ColorsCustom.secondaryColor,
                                              fontSize: 16.0),
                                        ),
                                        Text.rich(TextSpan(children: [
                                          TextSpan(
                                            text: '${widget.product.prix} FCFA',
                                            style: TextStyle(
                                                decoration:
                                                    TextDecoration.lineThrough,
                                                color: Colors.grey,
                                                fontSize: 12.0),
                                          ),
                                          TextSpan(
                                            text:
                                                '  ${(((widget.product.prix - widget.product.discount) * 100) / widget.product.prix).round() * -1}%',
                                            style: TextStyle(fontSize: 12.0),
                                          )
                                        ]))
                                      ],
                                    )
                                  : Text(
                                      '${widget.product.prix} FCFA',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: ColorsCustom.secondaryColor,
                                          fontSize: 16.0),
                                    ),
                            ),
                            BlocBuilder(
                              bloc: _wishlistBloc,
                              builder: (BuildContext context,
                                  WishlistState wishliststate) {
                                if (wishliststate is WishlistLoaded &&
                                    wishliststate.produits.firstWhere(
                                            (test) =>
                                                test.id == widget.product.id,
                                            orElse: () => null) !=
                                        null) {
                                  return Padding(
                                    padding: const EdgeInsets.all(0.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        IconButton(
                                          icon: Icon(
                                            EvaIcons.heart,
                                            color: ColorsCustom.secondaryColor,
                                          ),
                                          onPressed: () {
                                            _wishlistBloc.dispatch(
                                                RemoveWish(widget.product));
                                          },
                                        ),
                                        IconButton(
                                          icon: Icon(
                                            EvaIcons.shareOutline,
                                            color: ColorsCustom.primaryColor,
                                          ),
                                          onPressed: () {
                                            var link = widget.product.nom
                                                .splitMapJoin(RegExp(r' '),
                                                    onMatch: ((m) => '-'));

                                            Share.share('http://shyn-e.net/' +
                                                link +
                                                '.html');
                                          },
                                        )
                                      ],
                                    ),
                                  );
                                } else if (wishliststate is WishlistLoaded &&
                                    wishliststate.produits.firstWhere(
                                            (test) =>
                                                test.id == widget.product.id,
                                            orElse: () => null) ==
                                        null) {
                                  return Padding(
                                    padding: const EdgeInsets.all(0.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        IconButton(
                                          icon: Icon(
                                            EvaIcons.heartOutline,
                                            color: ColorsCustom.primaryColor,
                                          ),
                                          onPressed: () {
                                            _wishlistBloc.dispatch(
                                                AddWish(widget.product));
                                          },
                                        ),
                                        IconButton(
                                          icon: Icon(
                                            EvaIcons.shareOutline,
                                            color: ColorsCustom.primaryColor,
                                          ),
                                          onPressed: () {
                                            var link = widget.product.nom
                                                .splitMapJoin(RegExp(r' '),
                                                    onMatch: ((m) => '-'));

                                            Share.share('http://shyn-e.net/' +
                                                link +
                                                '.html');
                                          },
                                        )
                                      ],
                                    ),
                                  );
                                } else {
                                  return Container();
                                }
                              },
                            )
                          ],
                        ),
                        SizedBox(
                          height: 6.0,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0, left: 8.0),
                          child: Text(widget.product.nom,
                              style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(
                          height: 4.0,
                        ),
                        Container(
                          width: double.infinity,
                          color: Colors.grey[100],
                          padding: EdgeInsets.all(8.0),
                          margin: EdgeInsets.only(top: 8.0),
                          child: Text(
                            'Description',
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(8.0),
                          child: Text(widget.product.description),
                        ),
                        Container(
                          width: double.infinity,
                          color: Colors.grey[100],
                          padding: EdgeInsets.all(8.0),
                          margin: EdgeInsets.only(top: 8.0),
                          child: Text(
                            'Détails',
                          ),
                        ),
                        Html(
                          padding: EdgeInsets.all(8.0),
                          data: widget.product.caracteristique,
                        ),
                        Container(
                          width: double.infinity,
                          color: Colors.grey[100],
                          padding: EdgeInsets.all(8.0),
                          margin: EdgeInsets.only(top: 8.0),
                          child: Text(
                            'Livraison et retours',
                          ),
                        ),
                        Card(
                          elevation: 2,
                          child: Container(
                            height: 60,
                            padding: EdgeInsets.all(8),
                            color: Colors.white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Icon(EvaIcons.carOutline),
                                SizedBox(
                                  width: 300,
                                  child: AutoSizeText(
                                    'Les livraisons se font entre 8h et 17h les jours ouvrables patout a Abidjan.Prevoir  entre 1 et 2 jours pour la livraison',
                                    maxLines: 4,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Card(
                          elevation: 2,
                          child: Container(
                            height: 40,
                            padding: EdgeInsets.all(8),
                            color: Colors.white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Icon(EvaIcons.refreshOutline),
                                SizedBox(
                                  width: 300,
                                  child: AutoSizeText(
                                    'Retours gratuits sous 10 jours',
                                    maxLines: 4,
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ))
                  ],
                ),
                state is LoadingState ? _progressHUD : Container()
              ],
            );
          },
        ),
        bottomNavigationBar: BlocBuilder(
          bloc: _cartBloc,
          builder: (BuildContext context, CartState state) {
            return Builder(
              builder: (BuildContext context) {
                return Container(
                    height: 50,
                    color: ColorsCustom.primaryColor,
                    padding: EdgeInsets.all(8.0),
                    child: InkWell(
                        highlightColor: ColorsCustom.secondaryColor,
                        onTap: () {
                          if (_authentificationBloc.currentState
                              is Authenticated) {
                            print(widget.product.id);
                            if (state is LoadedState) {
                              if (state.cartItems.indexWhere((item) =>
                                      item.item_id == widget.product.id) ==
                                  -1) {
                                _cartBloc.dispatch(Add(widget.product));
                                setState(() {
                                  firtsload = false;
                                });
                              } else {
                                var index = state.cartItems.indexWhere((item) =>
                                    item.item_id == widget.product.id);
                                _cartBloc.dispatch(
                                    UpdateNoImage(state.cartItems[index]));
                                setState(() {
                                  firtsload = false;
                                });
                              }
                            }
                          } else {
                            Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => LoginPage()))
                                .then((loged) {
                              if (loged) {
                                _cartBloc.dispatch(FetchNoImages());
                              }
                            });
                          }
                        },
                        child: Center(
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Icon(
                                EvaIcons.shoppingCartOutline,
                                color: Colors.white,
                              ),
                              Text(
                                'Ajouter au panier',
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                        )));
              },
            );
          },
        ));
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }
}
