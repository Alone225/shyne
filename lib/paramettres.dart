import 'package:flutter/material.dart';

class Parametres extends StatefulWidget {
  @override
  _ParametresState createState() => _ParametresState();
}

class _ParametresState extends State<Parametres> {
  bool receive=false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Parametres'),

      ),
      body: Container(
            color: Colors.white,
            child: ListTile(
              
              title: Text("Recevoir les notifications Shyn-e", style: TextStyle(color: Colors.grey,fontSize: 12.0),),
              trailing: Switch(
                
                onChanged: (value) {
                  setState(() {
                   receive=value; 
                  });
                },
                 value:receive,

              ),
            ),
          )
    );
  }
}