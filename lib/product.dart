class Produit {
  int id;
  String nom;
  int prix;
  String src;
  String sku;
  String description;
  String caracteristique;
  int discount;
  Produit(
      {this.id,
      this.sku,
      this.nom,
      this.prix,
      this.src,
      this.description,
      this.caracteristique,
      this.discount});

  factory Produit.fromJson(Map<String, dynamic> item) {
    final String imgbaseUrl = "http://shyn-e.net/pub/media/catalog/product";
    var src = item['custom_attributes']
        .where((at) => at['attribute_code'] == 'image')
        .first;
    var description = item['custom_attributes']
        .where((at) => at['attribute_code'] == 'meta_description')
        .first;
    var features = item['custom_attributes']
        .where((at) => at['attribute_code'] == 'features')
        .first;
    var specialprice = item['custom_attributes'].firstWhere(
        (at) => at['attribute_code'] == 'special_price',
        orElse: () => {"attribute_code": "special_price", "value": "0.0"});
    print(specialprice);
    return Produit(
        id: item['id'],
        nom: item['name'],
        prix: item['price'],
        sku: item['sku'],
        src: imgbaseUrl + src['value'],
        caracteristique: features['value'],
        description: description['value'],
        discount: double.parse(specialprice['value']).round());
  }
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'nom': nom,
      'prix': prix,
      'sku': sku,
      'src': src,
      'caracteristique': caracteristique,
      'description': description,
      'discount': discount
    };
  }

  factory Produit.fromLocal(Map<String, dynamic> json) {
    return Produit(
        id: json['id'],
        nom: json['nom'],
        prix: json['prix'],
        sku: json['sku'],
        src: json['src'],
        caracteristique: json['caracteristique'],
        description: json['description'],
        discount: json['discount']);
  }
}
