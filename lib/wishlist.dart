import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:progress_hud/progress_hud.dart';
import 'package:shyne/blocs/cartbloc/bloc.dart';
import 'package:shyne/blocs/wishlistbloc/bloc.dart';
import 'package:shyne/cartbutton.dart';
import 'package:shyne/utils/colors.dart';

class WishList extends StatefulWidget {
  @override
  _WishListState createState() => _WishListState();
}

class _WishListState extends State<WishList> {
  WishlistBloc _wishlistBloc;
  CartBloc _cartBloc;
  ProgressHUD _progressHUD;
  @override
  void initState() {
    _wishlistBloc = BlocProvider.of<WishlistBloc>(context);
    _cartBloc = BlocProvider.of<CartBloc>(context);

    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: Colors.orangeAccent,
      containerColor: Colors.white,
      loading: true,
      borderRadius: 10.0,
      text: '',
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Liste de souhait'),
        actions: <Widget>[
          CartButton()
        ],
      ),
      body: BlocBuilder(
        bloc: _wishlistBloc,
        builder: (BuildContext context, WishlistState wishliststate) {
          return BlocBuilder(
            bloc: _cartBloc,
            builder: (BuildContext context, CartState cartstate) {
              if (wishliststate is WishlistLoaded &&
                  wishliststate.produits.isNotEmpty) {
                return Stack(
                  children: <Widget>[
                    SingleChildScrollView(
                      child: Column(
                        children: wishliststate.produits.map((produit) {
                          return Row(
                            children: <Widget>[
                              Container(
                                  width: MediaQuery.of(context).size.width,
                                  color: Colors.white,
                                  margin: EdgeInsets.only(bottom: 1.0),
                                  padding:
                                      EdgeInsets.only(left: 2.0, right: 2.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.all(6.0),
                                        child: Image.network(
                                          produit.src,
                                          width: 80.0,
                                          height: 90.0,
                                          fit: BoxFit.fitHeight,
                                        ),
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                              child: Container(
                                            width: 230,
                                            child: Text(
                                              produit.nom,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w200),
                                            ),
                                          )),
                                          Text(
                                            '${produit.discount > 0 ? produit.discount : produit.prix} FCFA',
                                            style: TextStyle(
                                                color:
                                                    ColorsCustom.secondaryColor,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          SizedBox(
                                            height: 4,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              FlatButton(
                                                color:
                                                    ColorsCustom.secondaryColor,
                                                textColor: Colors.white,
                                                child:
                                                    Text('Ajouter au panier'),
                                                onPressed: () {
                                                  if (cartstate
                                                      is LoadedState) {
                                                    if (cartstate.cartItems
                                                            .indexWhere((item) =>
                                                                item.item_id ==
                                                                produit.id) ==
                                                        -1) {
                                                      _cartBloc.dispatch(
                                                          Add(produit));
                                                    } else {
                                                      var index = cartstate
                                                          .cartItems
                                                          .indexWhere((item) =>
                                                              item.item_id ==
                                                              produit.id);
                                                      _cartBloc.dispatch(
                                                          UpdateNoImage(cartstate
                                                                  .cartItems[
                                                              index]));
                                                    }
                                                  }
                                                },
                                              ),
                                              SizedBox(
                                                width: 50,
                                              ),
                                              IconButton(
                                                icon: Icon(Icons.remove_circle),
                                                onPressed: () {
                                                  _wishlistBloc.dispatch(
                                                      RemoveWish(produit));
                                                },
                                              )
                                            ],
                                          )
                                        ],
                                      )
                                    ],
                                  ))
                            ],
                          );
                        }).toList(),
                      ),
                    ),
                    cartstate is LoadingState ? _progressHUD : Container()
                  ],
                );
              } else {
                return Center(
                  child: Text(' Votre liste de souhait est vide'),
                );
              }
            },
          );
        },
      ),
    );
  }
}
