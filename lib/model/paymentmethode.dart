class PaymentMethode {
  String code;
  String title;

  PaymentMethode({this.code, this.title});

factory PaymentMethode.fromJson(Map<String, dynamic> data){
  return PaymentMethode(
    code: data['code'],
    title: data['title']
  );
}
}