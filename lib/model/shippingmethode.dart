class ShippingMethode {
  String carriercode;
  String methodcode;
  String carriertitle;
  String methodtitle;
  int amount;

  ShippingMethode({this.carriercode, this.methodcode, this.carriertitle, this.methodtitle, this.amount});


  factory ShippingMethode.fromJson( Map<String, dynamic> data){
    return ShippingMethode(
      carriercode: data['carrier_code'],
      methodcode: data['method_code'],
      carriertitle: data['carrier_title'],
      methodtitle: data['method_title'],
      amount: data['amount']
    );
  }
}
