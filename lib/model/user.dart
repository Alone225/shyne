import 'package:scoped_model/scoped_model.dart';

class User extends Model{
  int id =0;
  String firstname='';
  String lastname='';
  String email='';
  String tokken='';
  

  User ({this.id, this.firstname, this.lastname,this.email, this.tokken}) ;
  User.fromJson(Map<String , dynamic> data, String tokken):
    id=data['id'],
    firstname=data['firstname'],
    lastname=data['lastname'],
    email=data['email'],
    tokken=tokken;
}