class Adresse {
  int id;
  String firstname;
  String lastname;
  String telephone;
  String city;
  List<String> adresse;
  String postcode;
  String country;
  String email;
  int regionid;

  Adresse.fromJson(Map<String, dynamic> data)
      : id = data['id'],
        firstname = data['firstname'],
        lastname = data['lastname'],
        telephone = data['telephone'],
        city = data['city'],
        adresse = getStreets(data['street']),
        postcode = data['postcode'],
        country = data['country_id'],
        regionid = data['region_id'];

  Adresse();
  static getStreets(List streets) {
    List<String> stree = List();
    streets.forEach((f) {
      stree.add(f);
    });
    return stree;
  }
}
