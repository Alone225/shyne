
import 'package:shyne/api/magento.dart';

class CartItem  {
  int item_id;
  String sku;
  int qty;
  String name;
  int price;
  String quote_id;
  List<String> images;
  
  
  
  CartItem.fromJson(Map<String, dynamic> item):
    item_id=item['item_id'],
    sku=item['sku'],
    qty=item['qty'],
    name=item['name'],
    price=item['price'],
    quote_id=item['quote_id'],
    images= [];
    


getImages() async{
  Magento magento =Magento();
 
  await magento.getProcutImages(sku).then((imgs){
    images.addAll(imgs);
  });

  return images;
}

  
}