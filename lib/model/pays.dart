class Pays {
  String id;
  String abrshort;
  String abrlong;
  String namelocale;
  String nameeng;

  Pays({this.id, this.abrshort, this.abrlong, this.namelocale, this.nameeng});

  factory Pays.fromJson( Map<String, dynamic> data){
    return Pays(
      id: data['id'],
      abrshort: data['two_letter_abbreviation'],
      abrlong: data['three_letter_abbreviation'],
      namelocale: data['full_name_locale'],
      nameeng: data['full_name_english']
    );
  }
}