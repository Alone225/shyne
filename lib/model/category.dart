import 'package:scoped_model/scoped_model.dart';

class Category extends Model{
  int id;
  String name;
  int product_count;
  List<Category> subs;

  Category.fromJson(Map<String, dynamic> data):
    id=data['id'],
    name=data['name'],
    product_count=data['product_count'],
    subs= _getSubs(data['children_data']);

  static List<Category>  _getSubs(List subs){
    List<Category> list=List();
    List datas=subs??List();
    datas.forEach((f){
      list.add(Category.fromJson(f));
    });
    return list;
  }
}