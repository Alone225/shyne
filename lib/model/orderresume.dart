import 'package:shyne/model/paymentmethode.dart';

class OrderResume {
  List<PaymentMethode> payments=List();
  Total total=Total();
  }
  
class Total {
  int grandtotal;
  int discount;
  int shippingamount;
  int itemsqty;

  Total({this.grandtotal, this.discount, this.shippingamount, this.itemsqty});
  factory Total.fromJson(Map<String, dynamic> data){
    return Total(
      grandtotal: data['grand_total'],
      discount: data['discount_amount'],
      shippingamount: data['shipping_amount'],
      itemsqty: data['items_qty']
    );
  }
}