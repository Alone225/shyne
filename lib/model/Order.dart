import 'package:shyne/model/adresse.dart';

class Order {
  int id;
  List<Item> items = List();
  String ref;
  String status;
  String date;
  String paiement;
  String livraison;
  Adresse adresse;
  int soustotal;
  int montantglobal;
  int shippingcost;
  int reduction;
  int itemcount;

  Order(
      {
        this.id,
        this.items,
      this.ref,
      this.status,
      this.date,
      this.paiement,
      this.adresse,
      this.soustotal,
      this.montantglobal,
      this.shippingcost,
      this.reduction,
      this.itemcount,
      this.livraison});

  factory Order.fromJson(Map<String, dynamic> data) {
    return Order(
      id: data['entity_id'],
      items: getItems(data['items']),
      ref: data['increment_id'],
      status: data['status'],
      date: data['created_at'],
      paiement: data['payment']['additional_information'].first,
      adresse: Adresse.fromJson(data['billing_address']),
      soustotal: data['subtotal'],
      montantglobal: data['total_due'],
      shippingcost: data['shipping_amount'],
      reduction: data['discount_amount'],
      itemcount: data['total_qty_ordered'],
      livraison: data['shipping_description']
    );
  }
}

List<Item> getItems(List data) {
  List<Item> items=[];
  data.forEach((item){
    items.add(Item.fromJson(item));
  });
  return items;
}

class Item {
  String name;
  int price;
  int qty;
  int soustotal;

  Item({this.name, this.price, this.qty, this.soustotal});

  factory Item.fromJson(Map<String, dynamic> data) {
    return Item(
        name: data['name'],
        price: data['price'],
        qty: data['qty_ordered'],
        soustotal: data['base_row_total']);
  }
}
