import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:shyne/msgcommandes.dart';
import 'package:shyne/msgpromo.dart';
import 'package:shyne/msgretours.dart';

class Messages extends StatefulWidget {
  @override
  _MessagesState createState() => _MessagesState();
}

class _MessagesState extends State<Messages> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Messages'),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            ListTile(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=> MsgRetour()));
              },
              leading: Icon(EvaIcons.gift, color: Colors.purple),
              title: Text('Promos'),
              trailing: Icon(Icons.keyboard_arrow_right, color: Colors.grey[400]),
            ),
            ListTile(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=> MsgCommande()));
              },
              leading: Icon(EvaIcons.archive, color: Colors.orange),
              title: Text('Commandes'),
              trailing: Icon(Icons.keyboard_arrow_right, color: Colors.grey[400]),
            ),
            
            ListTile(
               onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=> MsgPromo()));
              },
              leading: Icon(EvaIcons.bell, color: Colors.blue),
              title: Text('Notifications'),
              trailing: Icon(Icons.keyboard_arrow_right, color: Colors.grey[400]),
            ),

            Container(
              padding: EdgeInsets.all(8.0),
              child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Supers deals pour le nouvel an',),
                     Text('01/01/2019',style: TextStyle(fontSize: 14.0,color: Colors.black45 ))
                  ],
                ),
                Text("Commancez l'année en beauté. Faites les boutiques maintenant et recevez jusqu'à 80% de réduction",
                style: TextStyle(fontSize: 14.0,color: Colors.black45),
                )
              ],
            )
            )
          ],
        ),
      ),
    );
  }
}