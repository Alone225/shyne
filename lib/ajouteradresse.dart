import 'package:flutter/material.dart';
import 'package:progress_hud/progress_hud.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/model/adresse.dart';
import 'package:shyne/model/pays.dart';
import 'package:shyne/model/user.dart';
import 'package:shyne/utils/colors.dart';

class AjouterAdresse extends StatefulWidget {
  @override
  _AjouterAdresseState createState() => _AjouterAdresseState();
}

class _AjouterAdresseState extends State<AjouterAdresse> {
  Magento magento = Magento();
  List<Pays> pays = List();
  Pays _pays = Pays();

  ProgressHUD _progressHUD;

  TextEditingController nom;
  TextEditingController prenom;
  TextEditingController telephone;
  TextEditingController adresse;
  TextEditingController ville;
  TextEditingController codepostal;
  User user = User();
  bool loading = true;
  getCountries() async {
    var countries = await magento.getCountries();
    var _user = await magento.getUser();
    setState(() {
      pays.addAll(countries);
      countries.forEach((f) => print(f.id));
      _pays = pays.firstWhere((it) => it.id == "CI");
      user = _user;
      nom.text = user.firstname;
      prenom.text = user.lastname;
      print(_pays.namelocale);
      loading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    nom = TextEditingController();
    prenom = TextEditingController();
    telephone = TextEditingController();
    adresse = TextEditingController();
    ville = TextEditingController();
    codepostal = TextEditingController();
    getCountries();
    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: ColorsCustom.secondaryColor,
      containerColor: Colors.white,
      borderRadius: 5.0,
      loading: true,
      text: '',
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Ajouter mon adresse'),
        ),
        body: Builder(
          builder: (BuildContext context) {
            return Stack(
              children: <Widget>[
                SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Theme(
                        data: Theme.of(context).copyWith(
                          primaryColor: Color(0xFF374250),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 16.0, right: 16.0, top: 16),
                              child:Text.rich(TextSpan(children: [
                                  TextSpan(
                                    text: 'Nom'
                                  ),
                                  TextSpan(
                                    text: ' *',
                                    style: TextStyle(color: Colors.red)

                                  ),
                                ])),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                left: 16.0,
                                right: 16.0,
                              ),
                              child: TextField(
                                cursorColor: Color(0xFF374250),
                                controller: nom,
                                decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.only(top: 4.0, bottom: 4.0)),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 16.0, right: 16.0, top: 16),
                              child: 
                              Text.rich(TextSpan(children: [
                                  TextSpan(
                                    text: 'Prénom'
                                  ),
                                  TextSpan(
                                    text: ' *',
                                    style: TextStyle(color: Colors.red)

                                  ),
                                ])),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                left: 16.0,
                                right: 16.0,
                              ),
                              child: TextField(
                                cursorColor: Color(0xFF374250),
                                controller: prenom,
                                decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.only(top: 4.0, bottom: 4.0)),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 16.0, right: 16.0, top: 16),
                              child: 
                                  Text.rich(TextSpan(children: [
                                  TextSpan(
                                    text: 'Numéro de téléphone'
                                  ),
                                  TextSpan(
                                    text: ' *',
                                    style: TextStyle(color: Colors.red)

                                  ),
                                ])),
                            ),
                            
                            Padding(
                              padding: EdgeInsets.only(
                                left: 16.0,
                                right: 16.0,
                              ),
                              child: TextField(
                                cursorColor: Color(0xFF374250),
                                keyboardType: TextInputType.phone,
                                controller: telephone,
                                decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.only(top: 4.0, bottom: 4.0)),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 16.0, right: 16.0, top: 16.0),
                              child: Text.rich(TextSpan(children: [
                                  TextSpan(
                                    text: 'Adresse'
                                  ),
                                  TextSpan(
                                    text: ' *',
                                    style: TextStyle(color: Colors.red)

                                  ),
                                ]))
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 16.0, right: 16.0),
                              child: TextField(
                                cursorColor: Color(0xFF374250),
                                controller: adresse,
                                decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.only(top: 4.0, bottom: 4.0)),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 16.0, right: 16.0, top: 16.0),
                              child: Text.rich(TextSpan(children: [
                                  TextSpan(
                                    text: 'Code postal'
                                  ),
                                  TextSpan(
                                    text: ' *',
                                    style: TextStyle(color: Colors.red)

                                  ),
                                ]))
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 16.0, right: 16.0),
                              child: TextField(
                                cursorColor: Color(0xFF374250),
                                controller: codepostal,
                                decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.only(top: 4.0, bottom: 4.0)),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 16.0, right: 16.0, top: 16.0),
                              child: Text.rich(TextSpan(children: [
                                  TextSpan(
                                    text: 'Ville'
                                  ),
                                  TextSpan(
                                    text: ' *',
                                    style: TextStyle(color: Colors.red)

                                  ),
                                ]))
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 16.0, right: 16.0),
                              child: TextField(
                                cursorColor: Color(0xFF374250),
                                controller: ville,
                                decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.only(top: 4.0, bottom: 4.0)),
                              ),
                            ),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 16.0, right: 16.0, top: 16.0),
                                child: Text.rich(TextSpan(children: [
                                  TextSpan(
                                    text: 'Pays'
                                  ),
                                  TextSpan(
                                    text: ' *',
                                    style: TextStyle(color: Colors.red)

                                  ),
                                ]))),
                            pays.isEmpty
                                ? Container()
                                : Padding(
                                    padding: EdgeInsets.only(
                                        left: 16.0, right: 16.0),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton<Pays>(
                                        value: _pays,
                                        onChanged: (Pays newValue) {
                                          setState(() {
                                            _pays = newValue;
                                          });
                                        },
                                        items: pays.map<DropdownMenuItem<Pays>>(
                                            (Pays value) {
                                          return DropdownMenuItem<Pays>(
                                            value: value,
                                            child: Text(value.namelocale ??
                                                value.nameeng),
                                          );
                                        }).toList(),
                                      ),
                                    ))
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 4.0,
                      ),
                     Padding(
                       child:  Text(
                         
                        '(*) champs sont requis .',
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.red, fontSize: 12),
                      ),padding: EdgeInsets.all(8),
                     ),
                      SizedBox(
                        height: 18.0,
                      ),
                      ButtonTheme(
                        minWidth: 250.0,
                        height: 40.0,
                        child: RaisedButton(
                          onPressed: () {
                            if (nom.text.isNotEmpty &&
                                prenom.text.isNotEmpty &&
                                ville.text.isNotEmpty &&
                                this.adresse.text.isNotEmpty &&
                                telephone.text.isNotEmpty &&
                                codepostal.text.isNotEmpty) {
                              setState(() {
                                loading = true;
                              });
                              var adresse = Adresse();
                              setState(() {
                                adresse.firstname = nom.text;
                                adresse.lastname = prenom.text;
                                adresse.city = ville.text;
                                adresse.adresse = [this.adresse.text];
                                adresse.country = _pays.id;
                                adresse.telephone = telephone.text;
                                adresse.postcode = codepostal.text;
                              });
                              
                              magento
                                  .addAdresseUser(adresse, user)
                                  .then((succes) {
                                setState(() {
                                  loading = false;
                                });
                                if (succes) {
                                  Navigator.pop(context, true);
                                } else {
                                  Scaffold.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                          'Echec de la creation de l\'adresse'),
                                      backgroundColor: ColorsCustom.errorColor,
                                    ),
                                  );
                                }
                              });
                            } else {
                              Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text(
                                    'Remplissez tout les champs correctement'),
                                backgroundColor: ColorsCustom.errorColor,
                              ));
                            }
                          },
                          elevation: 0.0,
                          color: ColorsCustom.primaryColor,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          child: Text(
                            'Ajouter',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                loading ? _progressHUD : Container()
              ],
            );
          },
        ));
  }
}
