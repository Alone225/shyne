import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shyne/blocs/cartbloc/bloc.dart';
import 'package:shyne/cart.dart';
import 'package:shyne/utils/colors.dart';
import 'package:badges/badges.dart';


class CartButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
      var _cartBloc=BlocProvider.of<CartBloc>(context);
    return  BlocBuilder<CartEvent, CartState>(
      bloc: _cartBloc,
      builder: (BuildContext context,CartState state ){
       
        if ( state is LoadedState && state.cartItems.isNotEmpty) {
          return 
          BadgeIconButton(
                itemCount: state.productCount, // required
                icon: Icon(Icons.shopping_cart), // required
                badgeColor: ColorsCustom.secondaryColor, // default: Colors.red
                badgeTextColor: Colors.white, // default: Colors.white
                hideZeroCount: true, // default: true
                onPressed:  ()=> Navigator.push(context, MaterialPageRoute( builder: (context)=> Cart())));
        }else{
          return new IconButton(
                  icon: new Icon(
                    Icons.shopping_cart,
                    color: Colors.white,
                  ),
                  onPressed: ()=> Navigator.push(context, MaterialPageRoute( builder: (context)=> Cart())),
                );
        
        }
        
       
      }
    );
  }
}
