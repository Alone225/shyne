import 'dart:math';

import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:progress_hud/progress_hud.dart';
import 'package:shyne/api/magento.dart';
import 'package:shyne/blocproduits.dart';
import 'package:shyne/blocs/authentificationbloc/bloc.dart';
import 'package:shyne/blocs/cartbloc/bloc.dart';
import 'package:shyne/blocs/wishlistbloc/bloc.dart';
import 'package:shyne/cart.dart';
import 'package:shyne/cartbutton.dart';
import 'package:shyne/categories.dart';
import 'package:shyne/customprogress.dart';
import 'package:shyne/customui/OffersList.dart';
import 'package:shyne/customui/Smartphones.dart';
import 'package:shyne/local/persistservice.dart';
import 'package:shyne/login.dart';
import 'package:shyne/messages.dart';
import 'package:shyne/model/cartITem.dart';
import 'package:shyne/model/user.dart';
import 'package:shyne/moncompte.dart';
import 'package:shyne/product.dart';
import 'package:shyne/productdetails.dart';
import 'package:shyne/productsearch.dart';
import 'package:shyne/utils/colors.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProviderTree(
        blocProviders: [
          BlocProvider<CartBloc>(
              bloc: CartBloc(
                  magento: Magento(), persistService: PersistService())),
          BlocProvider<AuthentificationBloc>(
            bloc: AuthentificationBloc(
                magento: Magento(), persistService: PersistService()),
          ),
          BlocProvider<WishlistBloc>(
            bloc: WishlistBloc(PersistService()),
          )
        ],
        child: MaterialApp(
          title: 'Shyn-e',
          debugShowCheckedModeBanner: false,
          initialRoute: '/',
          theme: new ThemeData(
              primarySwatch: Colors.blue,
              primaryColor: ColorsCustom.primaryColor,
              fontFamily: 'OpenSans',
              textTheme: TextTheme(display4: TextStyle(fontSize: 8.0)),
              primaryColorBrightness: Brightness.dark,
              accentColor: ColorsCustom.secondaryColor,
              disabledColor: Colors.grey[400]),
          home: new MyHomePage(title: 'Shyn-e'),
        ));
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _random = new Random();
  ProgressHUD _progressHUD;

  bool _loading = true;
  static Options options =
      new Options(baseUrl: 'http://shyn-e.net/rest/all/V1/', headers: {
    'Authorization': ' Bearer ogqjk988owbmfw82chqslin0g54t3s5c',
    'accept': 'application/json'
  });
  final String imgbaseUrl = "http://shyn-e.net/pub/media/catalog/product";
  Dio dio = new Dio(options);
  List<Produit> _produits = List();
  List<Produit> _featured = List();
  Magento magento = Magento();

  List<CartItem> cartItems = List();
  int next(int min, int max) => min + _random.nextInt(max - min);
  User user = User();

  CartBloc _cartBloc;
  AuthentificationBloc _authentificationBloc;
  WishlistBloc _wishlistBloc;
  ScrollController _scrollController;
  bool firstpageloaded = false;
  int page = 1;
  bool loadingmore = false;
  bool containmore = true;

  Future getFeatured() async {
    await dio.get('categories/363/products', data: {
      'searchCriteria[pageSize]': 4,
      
    }).then((prods) {
      var data = prods.data;
      
      data.forEach((prod) {
        var sku = prod['sku'];
        dio.get('products/$sku').then((value) {
          var item = value.data;
          setState(() {
            _featured.add(Produit.fromJson(item));
          });
        });
      });
    });
  }

  

  Future getProducts() async {
    await dio.get('categories/2/products', data: {
      'searchCriteria[pageSize]': 10,
      
    }).then((prods) {
      var data = prods.data;
      print(data);
      data.forEach((prod) {
        var sku = prod['sku'];
        dio.get('products/$sku').then((value) {
          var item = value.data;
          setState(() {
            _produits.add(Produit.fromJson(item));
          });
        });
      });
    });
  }

  void onTabTapped(int index) {
    setState(() {
      print(index);
      if (index == 3) {
        if (_authentificationBloc.currentState is Authenticated) {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Compte()));
        } else {
          Navigator.of(context)
              .push(new MaterialPageRoute(builder: (context) => LoginPage()))
              .then((logedin) {
            if (logedin)
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Compte()));
          });
        }
      }
      if (index == 2) {
        if (_authentificationBloc.currentState is Authenticated) {
          Navigator.of(context)
              .push(new MaterialPageRoute(builder: (context) => Cart()));
        } else {
          Navigator.push(
                  context, MaterialPageRoute(builder: (context) => LoginPage()))
              .then((logedin) {
            if (logedin)
              Navigator.of(context)
                  .push(new MaterialPageRoute(builder: (context) => Cart()));
          });
        }
      }
      if (index == 1) {
        Navigator.of(context)
            .push(new MaterialPageRoute(builder: (context) => Messages()));
      }
    });
  }

  @override
  void initState() {
    _scrollController = ScrollController();
    _wishlistBloc = BlocProvider.of<WishlistBloc>(context);
    _cartBloc = BlocProvider.of<CartBloc>(context);
    _authentificationBloc = BlocProvider.of<AuthentificationBloc>(context);
    _wishlistBloc.dispatch(FisrtLoad());
    _authentificationBloc.dispatch(AppStarted());
    _cartBloc.dispatch(FetchNoImages());

    super.initState();
    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: Colors.deepOrangeAccent,
      containerColor: Colors.white,
      borderRadius: 5.0,
      text: 'Loading...',
    );

    Future.wait([
      getProducts(),
      getFeatured(),
    ], eagerError: true, cleanUp: (value) {})
        .then((data) {
      setState(() {
        firstpageloaded = true;
        if (_loading) {
          _progressHUD.state.dismiss();
        } else {
          _progressHUD.state.show();
        }

        _loading = !_loading;
      });
    }).catchError(print);
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
              _scrollController.position.maxScrollExtent &&
          firstpageloaded) {
        setState(() {
          page += 1;
        });
      }
    });
  }

  @override
  void dispose() {
    _cartBloc.dispose();
    _authentificationBloc.dispose();
    super.dispose();
  }

  Widget _globalCollectionGriElement(
      String name, String desccription, String src) {
    return Container(
        color: Colors.white,
        padding: EdgeInsets.all(4.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    name,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0),
                  ),
                  Text(desccription + ' FCFA')
                ],
              ),
            ),
            Image.network(src, width: 50.0, height: 50.0)
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.blueGrey[50],
        body: Stack(
          children: <Widget>[
            CustomScrollView(
              controller: _scrollController,
              slivers: <Widget>[
                SliverAppBar(
                  expandedHeight: 200.0,
                  floating: false,
                  pinned: true,
                  centerTitle: true,
                  titleSpacing: 0.0,
                  elevation: 0.0,
                  leading: IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Categories()));
                    },
                    icon: Icon(Icons.menu),
                  ),
                  actions: <Widget>[CartButton()],
                  title: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProductSearch()));
                      },
                      child: Container(
                          height: 35.0,
                          padding: EdgeInsets.all(4.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.0))),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Icon(
                                  Icons.search,
                                  color: Colors.grey,
                                ),
                                Expanded(
                                  child: Text(
                                    'Rechercher un produit',
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 14.0),
                                  ),
                                )
                              ]))),
                  flexibleSpace: FlexibleSpaceBar(
                      centerTitle: true,
                      background: new Carousel(
                        images: [
                          new NetworkImage(
                              'http://shyn-e.net/mobileimages/slide1.jpeg'),
                          new NetworkImage(
                              'http://shyn-e.net/mobileimages/slide2.jpeg'),
                          new NetworkImage(
                              'http://shyn-e.net/mobileimages/slide3.jpeg'),
                          new NetworkImage(
                              'http://shyn-e.net/mobileimages/slide4.jpeg')
                        ],
                        dotSize: 4.0,
                        dotSpacing: 12.0,
                        dotColor: Colors.white,
                        indicatorBgPadding: 2.0,
                        dotBgColor: Colors.transparent,
                      )),
                ),
                SliverToBoxAdapter(
                    child: Column(
                  children: <Widget>[
                    Container(
                      color: Colors.white,
                      padding: EdgeInsets.only(top: 12.0, bottom: 4.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProduits(
                                            title: 'Meilleures Ventes',
                                            catsIds: [354, 377, 378, 415],
                                          )));
                            },
                            child: Column(
                              children: <Widget>[
                                CircleAvatar(
                                  child: Image.asset(
                                    'assets/imgboutons.png',
                                  ),
                                  backgroundColor: Colors.white,
                                ),
                                Text(
                                  'Meuilleures\nVentes',
                                  style: TextStyle(fontSize: 12.0),
                                  textAlign: TextAlign.center,
                                )
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProduits(
                                            title: 'Produits vedettes',
                                            catsIds: [378, 392, 415, 450],
                                          )));
                            },
                            child: Column(
                              children: <Widget>[
                                CircleAvatar(
                                  child: Image.asset('assets/imgboutons.png'),
                                  backgroundColor: Colors.white,
                                ),
                                Text(
                                  'Produits\nVedettes',
                                  style: TextStyle(fontSize: 12.0),
                                  textAlign: TextAlign.center,
                                )
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProduits(
                                            title: 'Nouveaux produits',
                                            catsIds: [377, 378, 392, 415],
                                          )));
                            },
                            child: Column(
                              children: <Widget>[
                                CircleAvatar(
                                  child: Image.asset('assets/imgboutons.png'),
                                  backgroundColor: Colors.white,
                                ),
                                Text(
                                  'Nouveaux\nProduits',
                                  style: TextStyle(fontSize: 12.0),
                                  textAlign: TextAlign.center,
                                )
                              ],
                            ),
                          ),
                          GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Categories()));
                              },
                              child: Column(
                                children: <Widget>[
                                  CircleAvatar(
                                    child: Image.asset('assets/imgboutons.png'),
                                    backgroundColor: Colors.white,
                                  ),
                                  Text(
                                    'Catégories\n ',
                                    style: TextStyle(
                                      fontSize: 12.0,
                                    ),
                                    textAlign: TextAlign.center,
                                  )
                                ],
                              ))
                        ],
                      ),
                    ),
                  ],
                )),
                SliverToBoxAdapter(
                  child: Secondlist(),
                ),
                SliverToBoxAdapter(child: Seventhlist(produits: _featured.take(4))),
                SliverToBoxAdapter(
                  child: Container(
                    child: Text(
                      'PRODUITS RECENTS',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SliverPadding(
                    padding: const EdgeInsets.all(8.0),
                    sliver: SliverGrid.count(
                        crossAxisSpacing: 2.0,
                        crossAxisCount: 2,
                        childAspectRatio: 4.5 / 6.1,
                        children: _produits
                            .map((f) => GestureDetector(
                                  child: Card(
                                      elevation: 0.2,
                                      child: Stack(
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: <Widget>[
                                              AspectRatio(
                                                aspectRatio: 14.0 / 12.0,
                                                child: Image.network(
                                                  f.src,
                                                  fit: BoxFit.fitWidth,
                                                ),
                                              ),
                                              Expanded(
                                                child: Padding(
                                                  padding: EdgeInsets.all(4.0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      SizedBox(
                                                        height: 10.0,
                                                      ),
                                                      Text(
                                                        f.nom,
                                                        maxLines: 1,
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            fontSize: 12.0),
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                      ),
                                                      SizedBox(
                                                        height: 6.0,
                                                      ),
                                                      Text(
                                                        '${f.discount > 0 ? f.discount : f.prix}  FCFA',
                                                        style: TextStyle(
                                                            color: ColorsCustom
                                                                .secondaryColor,
                                                            fontSize: 16.0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w700),
                                                      ),
                                                      f.discount > 0
                                                          ? Text(
                                                              '${f.prix}  FCFA',
                                                              style: TextStyle(
                                                                  decoration:
                                                                      TextDecoration
                                                                          .lineThrough,
                                                                  color: Colors
                                                                      .grey),
                                                            )
                                                          : Container()
                                                    ],
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                          f.discount > 0
                                              ? Positioned(
                                                  right: 0,
                                                  child: Container(
                                                    width: 40,
                                                    height: 20,
                                                    child: Center(
                                                      child: Text(
                                                        '${(((f.prix - f.discount) * 100) / f.prix).round() * -1}%',
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 12.0),
                                                      ),
                                                    ),
                                                    decoration: BoxDecoration(
                                                        color: ColorsCustom
                                                            .discuntBadgeColor,
                                                        borderRadius:
                                                            BorderRadius.only(
                                                                topRight: Radius
                                                                    .circular(
                                                                        4))),
                                                  ),
                                                )
                                              : Container()
                                        ],
                                      )),
                                  onTap: () => Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) =>
                                              ProductDetails(f))),
                                ))
                            .toList())),
                SliverToBoxAdapter(
                    child:
                        loadingmore ? CustomProgressIndicator() : Container())
              ],
            ),
          ],
        ),
        bottomNavigationBar: Theme(
          data: Theme.of(context).copyWith(
              primaryColor: Colors.orange,
              textTheme: Theme.of(context)
                  .textTheme
                  .copyWith(caption: new TextStyle(color: Color(0xFF374250)))),
          child: BottomNavigationBar(
            onTap: onTabTapped, // new
            type: BottomNavigationBarType.fixed,
            fixedColor: ColorsCustom.secondaryColor,
            iconSize: 18.0,
            currentIndex: 0, // new
            items: [
              new BottomNavigationBarItem(
                icon: Icon(
                  EvaIcons.home,
                ),
                title: Text(
                  'Accueil',
                ),
              ),
              new BottomNavigationBarItem(
                  icon: Icon(
                    EvaIcons.messageCircle,
                  ),
                  title: Text(
                    'Messages',
                  )),
              new BottomNavigationBarItem(
                  icon: Icon(
                    EvaIcons.shoppingCart,
                  ),
                  title: Text(
                    'Panier',
                  )),
              new BottomNavigationBarItem(
                  icon: Icon(
                    EvaIcons.person,
                  ),
                  title: Text(
                    'Compte',
                  ))
            ],
          ),
        ));
  }
}
