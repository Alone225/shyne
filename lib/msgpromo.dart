import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';

class MsgPromo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notifications'),
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(EvaIcons.bell, size: 42, color: Colors.grey,),
            Text('Vous n\'avez pas  de notification',
            textAlign: TextAlign.center,
            style: TextStyle( color: Colors.grey),
            )
          ]
        ),
        )
      ),
    );
  }
}